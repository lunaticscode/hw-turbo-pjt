import { FC, useMemo } from "react";
import { BaseComponentProps } from "../../libs/types/common";
import { useTabsContext } from "../Tabs";
import { addSubCls } from "../../libs/utils/className";
interface TabPannelProps extends BaseComponentProps {
  index?: number;
}
const TabPannel: FC<TabPannelProps> = (props) => {
  const { children, index, parentCls } = props;
  const { tabIndex } = useTabsContext();
  const tabPannelCls = useMemo(
    () => addSubCls(`${parentCls} tabpannel`),
    [parentCls]
  );
  if (tabIndex !== index) return null;
  return <p className={tabPannelCls}>{children}</p>;
};
export default TabPannel;
