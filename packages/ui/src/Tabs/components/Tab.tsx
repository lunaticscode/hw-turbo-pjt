import { FC, useCallback, useEffect, useMemo } from "react";
import { BaseComponentProps } from "../../libs/types/common";
import { _addSubCls } from "../../libs/utils/className";
import { TabsValue, useTabsContext } from "../Tabs";

interface TabProps extends BaseComponentProps {
  value?: string | number;
  index?: number;
  disabled?: boolean;
  onClickTab?: (value?: TabsValue, index?: number) => void;
}

const Tab: FC<TabProps> = (props) => {
  const { children, value, index, disabled, parentCls } = props;
  const {
    onClickTab,
    value: valueContext,
    tabIndex,
    setTabIndex,
  } = useTabsContext();
  const handleClickTab = useCallback(() => {
    if (disabled) return;
    onClickTab?.(value, index);
  }, [disabled, value, index, onClickTab]);

  useEffect(() => {
    if (value === valueContext) setTabIndex?.(index ?? 0);
  }, [value, valueContext, setTabIndex, index]);

  const isActive = useMemo(
    //* uncontrolled 에서는 내부 index로 판단
    () => (valueContext ? valueContext === value : index === tabIndex),
    [value, valueContext, tabIndex, index]
  );

  const tabCls = useMemo(
    () =>
      _addSubCls(`${parentCls}__tab`, {
        disabled,
        active: isActive,
      }),
    [parentCls, disabled, isActive]
  );

  return (
    <div className={tabCls} onClick={handleClickTab}>
      {children}
    </div>
  );
};
export default Tab;
