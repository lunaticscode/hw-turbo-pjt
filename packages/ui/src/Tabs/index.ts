export { default } from "./Tabs";
export * from "./Tabs";
export { default as Tab } from "./components/Tab";
export * from "./components/Tab";
export { default as TabPannel } from "./components/TabPannel";
export * from "./components/TabPannel";
