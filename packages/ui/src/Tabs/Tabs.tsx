import {
  Children,
  forwardRef,
  useMemo,
  cloneElement,
  createContext,
  useContext,
  useCallback,
  useState,
  Dispatch,
  SetStateAction,
} from "react";
import { BaseComponentProps } from "../libs/types/common";
import { makePrefixCls } from "../libs/utils/className";
import useControlledValue from "../libs/hooks/useControlledValue";
import Tab from "./components/Tab";
import TabPannel from "./components/TabPannel";
import "./styles/index.scss";
export type TabsValue = string | number | undefined;

interface TabsProps extends BaseComponentProps {
  value?: TabsValue;
  defaultValue?: TabsValue;
  onChange?: (value: TabsValue, index: number | undefined) => void;
}

interface TabsContextProps extends TabsProps {
  onClickTab?: (value?: TabsValue, index?: number) => void;
  tabIndex?: number;
  setTabIndex?: Dispatch<SetStateAction<number>>;
}

const TabsContext = createContext<TabsContextProps>({
  onClickTab: () => {},
  tabIndex: 0,
});

export const useTabsContext = () => {
  const context = useContext(TabsContext);
  if (!context) {
    throw new Error("(!) TabsContext를 호출할 수 없는 범위입니다.");
  }
  return context;
};

interface ITabs
  extends React.ForwardRefExoticComponent<
    TabsProps & React.RefAttributes<HTMLDivElement>
  > {
  Tab: typeof Tab;
  TabPannel: typeof TabPannel;
}

const TabsForwardRef = forwardRef<HTMLDivElement, TabsProps>((props, ref) => {
  const {
    children,
    className,
    value: valueProp,
    defaultValue: defaultValueProp,
    onChange,
  } = props;

  const [value, setValue] = useControlledValue(valueProp, defaultValueProp);
  const [tabIndex, setTabIndex] = useState(0);

  const _children = useMemo(
    () => Children.toArray(children) as JSX.Element[],
    [children]
  );

  const [tabElems, tabPannelElems] = useMemo(
    () => [
      _children.filter((elem) => elem.type === Tab),
      _children.filter((elem) => elem.type === TabPannel),
    ],
    [_children]
  );

  const onClickTab = useCallback(
    (value?: TabsValue, index?: number) => {
      console.log({ value, index });
      setValue(value);
      onChange?.(value, index);
      setTabIndex(index ?? 0);
    },
    [setValue, onChange]
  );

  const baseCls = makePrefixCls("tabs");
  const tabsCls = className ? `${className} ${baseCls}` : baseCls;
  const tabsWrapperCls = useMemo(() => `${tabsCls}__tabs-wrapper`, [tabsCls]);
  const tabPannelWrapperCls = useMemo(
    () => `${tabsCls}__tab-pannel-wrapper`,
    [tabsCls]
  );

  const contextValue: TabsContextProps = {
    value,
    tabIndex,
    setTabIndex,
    onClickTab,
  };

  return (
    <TabsContext.Provider value={contextValue}>
      <div className={baseCls} ref={ref}>
        <div className={tabsWrapperCls}>
          {tabElems?.map((tab, index) =>
            cloneElement(tab, {
              ...tab.props,
              key: `tab-${index}`,
              index,
              parentCls: tabsWrapperCls,
            })
          )}
        </div>
        <div className={tabPannelWrapperCls}>
          {tabPannelElems?.map((tabPannel, index) =>
            cloneElement(tabPannel, {
              ...tabPannel.props,
              key: `tab-pannel-${index}`,
              index,
              parentCls: tabPannelWrapperCls,
            })
          )}
        </div>
      </div>
    </TabsContext.Provider>
  );
});

const Tabs = {
  ...TabsForwardRef,
  Tab: Tab,
  TabPannel: TabPannel,
} as ITabs;

export default Tabs;
TabsForwardRef.displayName = "Tabs";
