import { FC } from "react";
import { BaseComponentProps } from "../../libs/types/common";
import { DropdownItemValue, useDropdownContext } from "../Dropdown";

export interface DropdownItemProps extends BaseComponentProps {
  index?: number;
  value?: DropdownItemValue;
}
const DropdownItem: FC<DropdownItemProps> = (props) => {
  const { children, value, index } = props;
  const { handleClickItem } = useDropdownContext();
  return <div onClick={() => handleClickItem?.(value, index)}>{children}</div>;
};
export default DropdownItem;
