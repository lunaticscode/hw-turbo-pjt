import { FC, forwardRef, useContext } from "react";
import { BaseComponentProps } from "../../libs/types/common";
import Button from "../../Button/Button";
import { useDropdownContext } from "../Dropdown";

export interface DropdownButtonProps extends BaseComponentProps {}
const DropdownButton = forwardRef<HTMLDivElement, DropdownButtonProps>(
  (props, ref) => {
    const { children = "Dropdown" } = props;
    const { open, setOpen } = useDropdownContext();
    return (
      <div ref={ref} style={{ display: "inline-block" }}>
        <Button onClick={() => setOpen?.(!open)}>{children}</Button>
      </div>
    );
  }
);

DropdownButton.displayName = "DropdownButton";

export default DropdownButton;
