import {
  Children,
  Dispatch,
  FC,
  SetStateAction,
  cloneElement,
  createContext,
  useContext,
  useMemo,
  useRef,
  useState,
} from "react";
import { BaseComponentProps } from "../libs/types/common";
import DropdownButton from "./components/DropdownButton";
import DropdownItem from "./components/DropdownItem";
import Popover from "../Popover/Popover";
import { makePrefixCls } from "../libs/utils/className";

interface DropdownContextProps {
  open?: boolean;
  setOpen?: Dispatch<SetStateAction<boolean>>;
  handleClickItem?: (value?: DropdownItemValue, index?: number) => void;
}
const DropdownContext = createContext<DropdownContextProps>({
  open: undefined,
  setOpen: undefined,
  handleClickItem: undefined,
});

export const useDropdownContext = () => {
  const context = useContext(DropdownContext);
  if (!context) {
    throw new Error("(!) DropdownContext를 호출할 수 없는 범위입니다.");
  }
  return context;
};

interface DropdownProps extends BaseComponentProps {
  onClickItem?: (value: DropdownItemValue, index: number) => void;
}
export type DropdownItemValue = string | undefined;

interface DropdownCompoundProps {
  Button: typeof DropdownButton;
  Item: typeof DropdownItem;
}

const baseCls = makePrefixCls("dropdown");
const Dropdown: FC<DropdownProps> & DropdownCompoundProps = (props) => {
  const { children, onClickItem } = props;
  const dropdownButtonRef = useRef<HTMLDivElement>(null);
  const _children = Children.toArray(children) as JSX.Element[];

  const [open, setOpen] = useState(false);

  // console.log(isPopoverOpen);

  const [dropdownButton, dropdownItems] = useMemo(
    () => [
      _children?.filter((child) => child.type === DropdownButton)[0] || null,
      _children?.filter((child) => child.type === DropdownItem) || null,
    ],
    [_children]
  );

  const handleClickItem = (value?: DropdownItemValue, index?: number) => {
    onClickItem?.(value, Number(index));
  };

  const contextValue: DropdownContextProps = {
    setOpen,
    open,
    handleClickItem,
  };

  const handleClickOutside = () => {
    // FIX ::: 이 지점 현재 두번씩 호출 됨(useDetectOutside 재점검)
    setOpen(false);
  };

  return (
    <DropdownContext.Provider value={contextValue}>
      <DropdownButton
        {...dropdownButton.props}
        ref={dropdownButtonRef}
        parentCls={baseCls}
      />
      <Popover
        anchorRef={dropdownButtonRef}
        open={open}
        onClickOutside={handleClickOutside}
      >
        <div>
          {dropdownItems.map((dropdownItems, index) => (
            <DropdownItem
              {...dropdownItems.props}
              key={`dropdown-item-${index}`}
              index={index}
            />
          ))}
        </div>
      </Popover>
    </DropdownContext.Provider>
  );
};

Dropdown.Button = DropdownButton;
Dropdown.Item = DropdownItem;

export default Dropdown;
