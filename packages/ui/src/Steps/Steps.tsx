import {
  createContext,
  Dispatch,
  forwardRef,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useState,
} from "react";
import StepItem, { StepItemProps } from "./components/StepItem";
import { BaseComponentProps } from "../libs/types/common";
import { makePrefixCls } from "../libs/utils/className";
import useControlledValue from "../libs/hooks/useControlledValue";

interface StepsContextProps extends Pick<StepsProps, "stepIndex"> {
  handleClickStep: (current: number) => void;
}

const StepsContext = createContext<StepsContextProps>({
  stepIndex: 0,
  handleClickStep: () => {},
});

export const useStepsContext = () => {
  const context = useContext(StepsContext);
  if (!context) {
    throw new Error("(!)StepsContext를 호출할 수 없는 범위입니다.");
  }
  return context;
};

export interface StepsProps extends BaseComponentProps {
  items?: Array<Omit<StepItemProps, "index">>;
  defaultStepIndex?: number;
  stepIndex?: number;
  progressIcon?: ReactNode;
  doneIcon?: ReactNode;
  onChange?: (stepIndex: number) => void;
}

const stepsClsName = "steps";
const Steps = forwardRef<HTMLUListElement, StepsProps>((props, ref) => {
  const {
    items,
    className,
    defaultStepIndex: defaultStepIndexProp = 0,
    stepIndex: stepIndexProp,
    onChange,
  } = props;
  // const [current, setCurrent] = useState<number>(currentProp);
  const [stepIndex, setStepIndex] = useControlledValue(
    stepIndexProp,
    defaultStepIndexProp
  );

  const baseCls = className
    ? `${className} ${makePrefixCls(stepsClsName)}`
    : makePrefixCls(stepsClsName);

  const handleClickStep = useCallback(
    (current: number) => {
      setStepIndex(current);
      onChange?.(current);
    },
    [onChange, setStepIndex]
  );

  const contextValue = {
    stepIndex,
    handleClickStep,
  };

  return (
    <StepsContext.Provider value={contextValue}>
      <ul className={baseCls} ref={ref}>
        {items?.map((itemProps, index) => {
          return (
            <StepItem
              {...itemProps}
              index={index}
              key={`step-${index}`}
              parentCls={baseCls}
            />
          );
        })}
      </ul>
    </StepsContext.Provider>
  );
});

export default Steps;
Steps.displayName = "Steps";
