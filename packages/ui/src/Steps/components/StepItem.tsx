import { FC, ReactNode, useCallback, useMemo } from "react";
import { BaseComponentProps } from "../../libs/types/common";
import { addSubCls } from "../../libs/utils/className";
import { useStepsContext } from "../Steps";

export interface StepItemProps extends BaseComponentProps {
  label: ReactNode;
  index: number;
  status?: "progress" | "finish" | "wait" | "error";
}
const StepItem: FC<StepItemProps> = (props) => {
  const { parentCls, label, status = "wait", index } = props;
  const { handleClickStep, stepIndex: stepIndexContext } = useStepsContext();

  const stepItemCls = useMemo(
    () =>
      addSubCls(`${parentCls}_step-item`, {
        active: stepIndexContext === index,
        [status]: true,
      }),
    [parentCls, status, stepIndexContext, index]
  );

  const onClickStepItem = useCallback(() => {
    handleClickStep?.(index);
  }, [handleClickStep, index]);

  return (
    <li className={stepItemCls} onClick={onClickStepItem}>
      {label}
    </li>
  );
};
export default StepItem;
