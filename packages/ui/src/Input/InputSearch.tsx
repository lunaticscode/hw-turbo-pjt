import { ChangeEvent, forwardRef, useCallback, useState } from "react";
import Button from "../Button";
import { makePrefixCls } from "../libs/utils/className";
import Input, { InputProps, InputValue } from "./Input";
interface InputSearchProps extends InputProps {
  buttonText?: string;
  onSearch?: (value: InputValue) => void;
}

const inputSearchWrapperCls = makePrefixCls("input-search-wrapper");
const InputSearch = forwardRef<HTMLDivElement, InputSearchProps>(
  (props, ref) => {
    const { onSearch, buttonText = "Search", ...rest } = props;
    const [inputValue, setInputValue] = useState<InputValue>("");

    const handleChangeInput = useCallback(
      (e: ChangeEvent<HTMLInputElement>, value: InputValue) => {
        setInputValue(value);
        rest.onChange?.(e, value);
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [rest.onChange]
    );

    const handleSearch = useCallback(() => {
      onSearch?.(inputValue);
    }, [onSearch, inputValue]);
    return (
      <div ref={ref} className={inputSearchWrapperCls}>
        <Input {...rest} onChange={handleChangeInput} role={"search"} />
        <Button onClick={handleSearch} variation={rest.variation}>
          {buttonText}
        </Button>
      </div>
    );
  }
);
export default InputSearch;
InputSearch.displayName = "InputSearch";
