/* eslint-disable react/display-name */
import { InputProps } from "../Input";
import { ChangeEvent, forwardRef, useCallback, useMemo } from "react";
import useControlledValue from "../../libs/hooks/useControlledValue";
import getInputByte from "../utils/getInputByte";
import { makePrefixCls } from "../../libs/utils/className";

const byteLimitList = [];
const convertStringToByte = (str: string) => {
  if (!str || !str.trim()) return 0;
  for (let i = 0; i < str.length; i++) {
    const char = str.charAt(i);
    if (encodeURIComponent(char)) {
    }
  }
  return 0;
};

const baseCls = makePrefixCls("input");
interface InputBaseProps extends InputProps {}
const InputBase = forwardRef<HTMLInputElement, InputBaseProps>((props, ref) => {
  const {
    className,
    defaultValue: defaultValueProp = "",
    value: valueProp,
    onChange,
    renderLength,
    renderByte,
  } = props;
  const [value, setValue] = useControlledValue(valueProp, defaultValueProp);

  const inputBaseCls = useMemo(() => className, [className]);

  const handleChangeInput = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const inputValue = e.currentTarget.value;
      setValue(inputValue);
      onChange?.(e, inputValue);
      getInputByte(inputValue);
    },
    [onChange, setValue]
  );

  const renderedLengthElem = useMemo(
    () => renderLength?.(value ? value.toString().length : 0) || null,
    [renderLength, value]
  );

  const renderedByteElem = useMemo(
    () => renderByte?.(convertStringToByte(value ? value.toString() : "")),
    [renderByte, value]
  );

  return (
    <>
      <div className={`${inputBaseCls}`}>
        <input value={value} onChange={handleChangeInput} ref={ref}></input>
      </div>
      {renderedLengthElem && (
        <div className={`${inputBaseCls} length-counter`}>
          {renderedLengthElem}
          {renderedByteElem}
        </div>
      )}
    </>
  );
});
export default InputBase;
