import { InputValue } from "../Input";

// const byteLimitList = [0, Math.pow(2, 7), Math.pow(2, 11), Math.pow(2, 15)];
const getInputByte = (value: InputValue) => {
  if (!value) return;
  const _value = value.toString();
  if (!_value.trim()) return;
  let byte = 0;
  //   console.time("log");
  for (let i = 0; i < _value.length; i++) {
    const asciiIndex = Number(encodeURIComponent(_value.charCodeAt(i))) || 0;
    byte = (byte + asciiIndex) >> 11 ? 3 : asciiIndex >> 7 ? 2 : 1;
  }
  //   console.timeEnd("log");
  return byte;
};
export default getInputByte;
