import {
  ChangeEvent,
  forwardRef,
  HTMLAttributes,
  ReactNode,
  useMemo,
} from "react";
import { addSubCls, makePrefixCls } from "../libs/utils/className";
import InputBase from "./components/InputBase";
import "./styles/index.scss";

export type InputValue = string | number;
type InputVariations = "basic" | "primary" | "danger" | "ghost";
export interface InputProps
  extends Omit<HTMLAttributes<HTMLInputElement>, "onChange" | "value"> {
  variation?: InputVariations;
  value?: InputValue;
  onChange?: (e: ChangeEvent<HTMLInputElement>, value: InputValue) => void;
  renderLength?: (length: number) => ReactNode;
  renderByte?: (byte: number) => ReactNode;
}

const Input = forwardRef<HTMLInputElement, InputProps>((props, ref) => {
  const { variation = "basic", renderLength, className, role, ...rest } = props;

  const cls = useMemo(
    () => makePrefixCls(role ? `input--${role}` : "input"),
    [role]
  );

  const inputCls = useMemo(
    () => addSubCls(cls, { [variation]: true }),
    [cls, variation]
  );

  return <InputBase {...rest} className={inputCls} ref={ref} />;
});
export default Input;
Input.displayName = "Input";
