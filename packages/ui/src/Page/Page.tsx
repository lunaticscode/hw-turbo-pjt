import { FC, PropsWithChildren, useEffect } from "react";
import { Events } from "../events";

interface PageProps extends PropsWithChildren {}

const Page: FC<PageProps> = ({ children }) => {
  useEffect(() => {
    Events.Page.load?.();
  }, []);
  return <div>{children}</div>;
};
export default Page;
