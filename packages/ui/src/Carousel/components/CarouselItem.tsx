import { CSSProperties, FC, useEffect, useMemo, useRef, useState } from "react";
import { BaseComponentProps } from "../../libs/types/common";
import { addSubCls, makePrefixCls } from "../../libs/utils/className";
import { useCarouselContext } from "./CarouselProvider";

const cls = makePrefixCls("carousel-item");
interface CarouselItemProps extends BaseComponentProps {
  index?: number;
  style?: CSSProperties;
}
const CarouselItem: FC<CarouselItemProps> = (props) => {
  const { children, index, style: styleProp = {} } = props;
  const { currentIndex, visibleIndexs } = useCarouselContext();

  const ref = useRef<HTMLDivElement>(null);

  const isVisible = useMemo(() => {
    return index !== undefined && visibleIndexs?.includes(index);
  }, [index, visibleIndexs]);

  const carouselItemCls = useMemo(
    () =>
      addSubCls(cls, {
        current_index: currentIndex === index,
      }),
    [currentIndex, index]
  );
  return isVisible ? (
    <div className={carouselItemCls} style={styleProp} ref={ref}>
      {children}
    </div>
  ) : null;
};
export default CarouselItem;
