import {
  FC,
  ForwardedRef,
  PropsWithChildren,
  createContext,
  useCallback,
  useContext,
  useImperativeHandle,
  useMemo,
  useState,
} from "react";

import { CarouselRefProps, CarouselRenderMode } from "../Carousel";

interface CarouselContextProps {
  itemLength?: number;
  currentIndex?: number;
  handleChangeIndex?: (index: number) => void;
  visibleIndexs?: Array<number>;
  slidesToShow?: number;
}

const CarouselContext = createContext<CarouselContextProps>({
  itemLength: undefined,
  currentIndex: undefined,
  handleChangeIndex: undefined,
  visibleIndexs: [],
  slidesToShow: undefined,
});

export const useCarouselContext = () => {
  const context = useContext(CarouselContext);
  if (!context) {
    throw new Error("(!) CarouselContext를 호출할 수 없는 범위 입니다.");
  }
  return context;
};

interface CarouselProviderProps extends PropsWithChildren {
  slidesToShow: number;
  itemLength: number;
  defaultIndex: number;
  renderMode?: CarouselRenderMode;
  carouselRef?: ForwardedRef<CarouselRefProps>;
  onChangeIndex: (index: number) => void;
}
const CarouselProvider: FC<CarouselProviderProps> = ({
  children,
  ...props
}) => {
  const {
    itemLength,
    defaultIndex,
    renderMode,
    carouselRef,
    slidesToShow = 1,
    onChangeIndex,
  } = props;
  const [currentIndex, setCurrentIndex] = useState<number>(defaultIndex);

  const handleChangeIndex = useCallback(
    (index: number) => {
      let visibleIndex = index;
      if (index < 0) {
        visibleIndex = Math.floor((itemLength - 1) / slidesToShow);
      } else {
        visibleIndex = index * slidesToShow > itemLength - 1 ? 0 : index;
      }
      setCurrentIndex(visibleIndex * slidesToShow);
      onChangeIndex(visibleIndex * slidesToShow);
    },
    [itemLength, onChangeIndex, slidesToShow]
  );

  useImperativeHandle(
    carouselRef,
    () => ({
      moveTo: (index: number) => handleChangeIndex(index),
      prev: () => handleChangeIndex(currentIndex - 1),
      next: () => handleChangeIndex(currentIndex + 1),
    }),
    [currentIndex, handleChangeIndex]
  );

  const visibleIndexs = useMemo(
    () =>
      renderMode === "required"
        ? [
            currentIndex - 1 < 0 ? itemLength - 1 : currentIndex - 1,
            Array.from(
              { length: slidesToShow },
              (_, index) => currentIndex + index
            ),
            currentIndex + (slidesToShow - 1) + 1 >= itemLength
              ? 0
              : currentIndex + 1,
          ].flat()
        : Array.from({ length: itemLength }, (_, index) => index),
    [currentIndex, itemLength, renderMode, slidesToShow]
  );

  const contextValue: CarouselContextProps = {
    itemLength,
    currentIndex,
    handleChangeIndex,
    visibleIndexs,
    slidesToShow,
  };

  return (
    <CarouselContext.Provider value={contextValue}>
      {children}
    </CarouselContext.Provider>
  );
};
export default CarouselProvider;
