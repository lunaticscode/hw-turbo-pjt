import { FC, useCallback, useContext } from "react";
import { useCarouselContext } from "./CarouselProvider";
import { addSubCls, makePrefixCls } from "../../libs/utils/className";

const cls = makePrefixCls("carousel-indicator");
interface CarouselIndicatorProps {}
const CarouselIndicator: FC<CarouselIndicatorProps> = () => {
  const {
    itemLength,
    currentIndex,
    slidesToShow = 1,
    handleChangeIndex,
  } = useCarouselContext();

  const handleClickIndicator = useCallback(
    (index: number) => {
      handleChangeIndex?.(index);
    },
    [handleChangeIndex]
  );

  return (
    <div className={cls}>
      {itemLength &&
        Array.from(
          { length: Math.floor(itemLength / slidesToShow) },
          (_, index) => (
            <div
              key={`carousel-indicator-point-${index}`}
              onClick={() => handleClickIndicator(index)}
              className={addSubCls(`${cls}-point`, {
                active: index * slidesToShow === currentIndex,
              })}
            />
          )
        )}
    </div>
  );
};
export default CarouselIndicator;
