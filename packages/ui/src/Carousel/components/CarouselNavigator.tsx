import { FC, useCallback } from "react";
import { useCarouselContext } from "./CarouselProvider";
import { makePrefixCls } from "../../libs/utils/className";
import ChevronLeft from "../../icons/ChevronLeft";
import ChevronRight from "../../icons/ChevronRight";

const cls = makePrefixCls("carousel-navigator");
interface CarouselNavigatorProps {}
const CarouselNavigator: FC<CarouselNavigatorProps> = () => {
  const { currentIndex, handleChangeIndex } = useCarouselContext();
  const handleClickNav = useCallback(
    (direction: number) => {
      handleChangeIndex?.((currentIndex || 0) + direction);
    },
    [currentIndex, handleChangeIndex]
  );
  return (
    <div className={cls}>
      <ChevronLeft onClick={() => handleClickNav(-1)} />
      <ChevronRight onClick={() => handleClickNav(1)} />
    </div>
  );
};
export default CarouselNavigator;
