import {
  Children,
  cloneElement,
  CSSProperties,
  forwardRef,
  ForwardRefExoticComponent,
  RefAttributes,
  useMemo,
  useState,
} from "react";

import { BaseComponentProps } from "../libs/types/common";
import CarouselIndicator from "./components/CarouselIndicator";
import CarouselItem from "./components/CarouselItem";
import CarouselNavigator from "./components/CarouselNavigator";
import CarouselProvider from "./components/CarouselProvider";
import { addSubCls, makePrefixCls } from "../libs/utils/className";
import "./styles/index.scss";
export type CarouselMode = "forced";
export type CarouselRenderMode = "all" | "required";
interface CarouselProps extends BaseComponentProps {
  mode?: CarouselMode;
  autoplay?: boolean;
  slidesToShow?: number; // 하나 슬라이드에서 보여줄 아이템 갯수 설정
  showNavigator?: boolean;
  showIndicator?: boolean;
  index?: number;
  defaultIndex?: number;
  width?: CSSProperties["width"];
  height?: CSSProperties["height"];
  renderMode?: CarouselRenderMode;
  bordered?: boolean;
  onChangeIndex?: (index: number) => void;
}

export interface CarouselRefProps {
  moveTo: (index: number) => void;
  prev: () => void;
  next: () => void;
}

const cls = makePrefixCls("carousel");
const CarouselForwardRef = forwardRef<CarouselRefProps, CarouselProps>(
  (props, ref) => {
    const {
      children,
      defaultIndex = 0,
      showIndicator = true,
      showNavigator = true,
      slidesToShow = 1,
      renderMode = "all",
      width = "350px",
      height = "150px",
      bordered = false,
    } = props;

    const [currentIndex, setCurrentIndex] = useState(defaultIndex);
    const childrenElems = Children.toArray(children) as JSX.Element[];
    // children 중에 CarouselItem 만 추출
    const onlyCarouselItems = useMemo(
      () => childrenElems.filter((child) => child.type === CarouselItem),
      [childrenElems]
    );

    const carouselCls = addSubCls(cls, { bordered });
    const validSlideToShow = useMemo(
      () =>
        slidesToShow > onlyCarouselItems.length
          ? onlyCarouselItems.length
          : slidesToShow,
      [slidesToShow, onlyCarouselItems]
    );

    return (
      <CarouselProvider
        onChangeIndex={(index) => setCurrentIndex(index)}
        slidesToShow={validSlideToShow}
        defaultIndex={defaultIndex}
        itemLength={onlyCarouselItems.length}
        renderMode={renderMode}
        carouselRef={ref}
      >
        <div className={carouselCls} style={{ width, height }}>
          {onlyCarouselItems?.map((carousel, index) => {
            const carouselItemStyle: CSSProperties = {
              transform: `translateX(calc(100% * ${index - currentIndex})`,
              transitionDuration: "1s",
              width: `calc(${width} / ${validSlideToShow})`,
              height: height,
            };
            return cloneElement(carousel, {
              ...carousel.props,
              index,
              style: carouselItemStyle,
            });
          })}
          {showIndicator && <CarouselIndicator />}
          {showNavigator && <CarouselNavigator />}
        </div>
      </CarouselProvider>
    );
  }
);

interface ICarousel
  extends ForwardRefExoticComponent<
    CarouselProps & RefAttributes<CarouselRefProps>
  > {
  Item: typeof CarouselItem;
}

const Carousel = {
  ...CarouselForwardRef,
  Item: CarouselItem,
} as ICarousel;

CarouselForwardRef.displayName = "Carousel";

export default Carousel;
