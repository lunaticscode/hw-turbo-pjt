import { FC, useMemo } from "react";
import { BaseComponentProps } from "../../libs/types/common";
interface AccordianContentProps extends BaseComponentProps {}
const AccordianContent: FC<AccordianContentProps> = (props) => {
  const { children, parentCls } = props;
  const contentCls = useMemo(() => `${parentCls}_content`, [parentCls]);
  return <div className={contentCls}>{children}</div>;
};
export default AccordianContent;
