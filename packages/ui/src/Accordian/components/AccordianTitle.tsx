import { FC, useMemo } from "react";
import { BaseComponentProps } from "../../libs/types/common";
interface AccordianTitleProps extends BaseComponentProps {}
const AccordianTitle: FC<AccordianTitleProps> = (props) => {
  const { children, parentCls } = props;
  const titleCls = useMemo(() => `${parentCls}_title`, [parentCls]);
  return <div className={titleCls}>{children}</div>;
};
export default AccordianTitle;
