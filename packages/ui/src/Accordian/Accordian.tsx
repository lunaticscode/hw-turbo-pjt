import {
  forwardRef,
  useState,
  ForwardRefExoticComponent,
  RefAttributes,
  Children,
  useMemo,
  cloneElement,
  useCallback,
} from "react";
import { BaseComponentProps } from "../libs/types/common";
import { addSubCls, makePrefixCls } from "../libs/utils/className";
import AccordianContent from "./components/AccordianContent";
import AccordianTitle from "./components/AccordianTitle";
export interface AccordianProps extends BaseComponentProps {
  collapsed?: boolean;
  onOpen?: () => void;
  disabled?: boolean;
}

const baseCls = makePrefixCls("accordian");
const AccordianForwardRef = forwardRef<HTMLDivElement, AccordianProps>(
  (props, ref) => {
    const { children, collapsed = true, disabled } = props;
    const [isCollapsed, setIsCollapsed] = useState(collapsed);

    const _children = useMemo(
      () => Children.toArray(children) as JSX.Element[],
      [children]
    );

    const [titleElem, contentElems] = useMemo(
      () => [
        _children.find((child) => child.type === AccordianTitle) as JSX.Element,
        _children.filter(
          (child) => child.type === AccordianContent
        ) as JSX.Element[],
      ],

      [_children]
    );

    const handleClickTitle = useCallback(() => {
      if (disabled) return;
      setIsCollapsed(!isCollapsed);
    }, [disabled, setIsCollapsed, isCollapsed]);

    const accordianCls = useMemo(
      () => addSubCls(baseCls, { disabled }),
      [disabled]
    );

    const [titleWrapperCls, contentsWrapperCls] = useMemo(
      () => [
        `${accordianCls} title-wrapper`,
        `${accordianCls} contents-wrapper`,
      ],
      [accordianCls]
    );

    return (
      <div ref={ref}>
        <div className={titleWrapperCls} onClick={handleClickTitle}>
          {cloneElement(titleElem, { parentCls: titleWrapperCls })}
        </div>
        <div className={contentsWrapperCls}>
          {!isCollapsed &&
            contentElems.map((elem, index) =>
              cloneElement(elem, {
                key: `accordian-content-${index}`,
                parentCls: contentsWrapperCls,
              })
            )}
        </div>
      </div>
    );
  }
);

interface IAccordian
  extends ForwardRefExoticComponent<
    AccordianProps & RefAttributes<HTMLDivElement>
  > {
  Title: typeof AccordianTitle;
  Content: typeof AccordianContent;
}

const Accordian = {
  ...AccordianForwardRef,
  Title: AccordianTitle,
  Content: AccordianContent,
} as IAccordian;

export default Accordian;
AccordianForwardRef.displayName = "Accordian";
