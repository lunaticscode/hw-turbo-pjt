/* eslint-disable react/display-name */
import React, {
  forwardRef,
  HTMLAttributes,
  MouseEvent,
  PropsWithChildren,
  useMemo,
} from "react";
import { Events } from "../events";
import { _addSubCls, makePrefixCls } from "../libs/utils/className";
import "./styles/index.scss";

type ButtonVariations = "basic" | "primary" | "danger" | "ghost";

interface ButtonProps
  extends HTMLAttributes<HTMLButtonElement>,
    PropsWithChildren {
  variation?: ButtonVariations;
}

// import {Button} from 'ui';

// <Button {...} />

const cls = makePrefixCls("button");
const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ variation = "basic", onClick, children, ...props }, ref) => {
    const handleClickButton = (e: MouseEvent<HTMLButtonElement>) => {
      Events.Button.click?.(e);
      onClick?.(e);
    };
    const buttonCls = useMemo(
      () => _addSubCls(cls, { [variation]: true }),
      [variation]
    );

    return (
      <button
        className={buttonCls}
        ref={ref}
        onClick={handleClickButton}
        {...props}
      >
        {children}
      </button>
    );
  }
);
export default Button;
