import { useMemo } from "react";
import useResize from "../../libs/hooks/useResize";
import { PopoverPositions } from "../Popover";

const getPopoverPostionValue = (
  tx: number,
  ty: number,
  tw: number,
  th: number,
  position: PopoverPositions
) => {
  const mapPostionTypeToPositionValue: Record<
    PopoverPositions,
    { px: number; py: number }
  > = {
    "bottom-left": { px: tx, py: ty + th },
    "bottom-right": { px: tw - tx, py: ty + th },
    "top-left": { px: tx, py: ty - th },
    "top-right": { px: tw - tx, py: ty - th },
  };
  return mapPostionTypeToPositionValue[position];
};

const useLocatePopover = (element: Element, position: PopoverPositions) => {
  const { width, height } = useResize();

  const tRect = useMemo(
    () => element?.getBoundingClientRect(),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [element, width, height]
  );

  const [tx, ty, tw, th] = [
    useMemo(() => tRect?.x || NaN, [tRect]),
    useMemo(() => tRect?.y || NaN, [tRect]),
    useMemo(() => tRect?.width || NaN, [tRect]),
    useMemo(() => tRect?.height || NaN, [tRect]),
  ];

  // popover.x & popover.y
  const { px, py } = useMemo(
    () => getPopoverPostionValue(tx, ty, tw, th, position),
    [tx, ty, tw, th, position]
  );

  return {
    px,
    py,
  };
};
export default useLocatePopover;
