import {
  FC,
  forwardRef,
  Ref,
  RefObject,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { createPortal } from "react-dom";
import useLocatePopover from "./hooks/useLocatePopover";
import { makePrefixCls } from "../libs/utils/className";
import { BaseComponentProps } from "../libs/types/common";
import useDetectOutside from "../libs/hooks/useDetectOutside";
export type PopoverPositions =
  | "bottom-left"
  | "bottom-right"
  | "top-left"
  | "top-right";

interface PopoverProps extends BaseComponentProps {
  anchorRef?: RefObject<HTMLElement>;
  anchorPosition?: PopoverPositions; // default "bottom-left"
  open?: boolean;
  onClickOutside?: () => void;
}

const baseCls = makePrefixCls("popover");

const Popover: FC<PopoverProps> = (props) => {
  const {
    anchorRef,
    anchorPosition = "bottom-left",
    open = false,
    children,
    className,
    onClickOutside,
  } = props;

  const [targetElem, setTargetElem] = useState<Element>(document.body);

  const { px, py } = useLocatePopover(
    anchorRef?.current as Element,
    anchorPosition
  );

  const { ref: popoverRef, isOutside } =
    useDetectOutside<HTMLDivElement>(anchorRef);

  useEffect(() => {
    if (anchorRef?.current) {
      setTargetElem(anchorRef.current as Element);
    }
  }, [anchorRef, popoverRef]);

  const popoverCls = useMemo(
    () => (className ? `${className} ${baseCls}` : baseCls),
    [className]
  );

  useEffect(() => {
    if (isOutside) {
      onClickOutside?.();
    }
  }, [isOutside, onClickOutside]);

  const handleFocus = () => {
    // console.log("focusfocus", open, isOutside);
  };

  const handleBlur = () => {
    // console.log("blurblur", isOutside);
    if (!open) return;
    if (isOutside) {
      onClickOutside?.();
    }
  };

  return open && !isNaN(px) && !isNaN(py)
    ? createPortal(
        <div
          tabIndex={0}
          onFocus={handleFocus}
          onBlur={handleBlur}
          className={popoverCls}
          style={{
            position: "absolute",
            top: `${py}px`,
            left: `${px}px`,
          }}
          ref={popoverRef}
        >
          {children}
        </div>,
        targetElem
      )
    : null;
};

export default Popover;
Popover.displayName = "Popover";
