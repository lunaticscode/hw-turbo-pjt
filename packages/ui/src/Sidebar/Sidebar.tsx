import { forwardRef } from "react";
interface SidebarProps {}

const Sidebar = forwardRef<HTMLDivElement, SidebarProps>((props, ref) => {
  return <div ref={ref}></div>;
});
export default Sidebar;
Sidebar.displayName = "Sidebar";
