import { FC, ReactNode } from "react";
import { useListContext } from "../";
interface ListItemProps {
  children?: ReactNode;
  id: string;
}
const ListItem: FC<ListItemProps> = (props) => {
  console.log(ListItem);
  const { children, id } = props;
  const { setSelectedId } = useListContext();
  const handleClickItem = () => {
    setSelectedId(id);
  };
  return <div onClick={handleClickItem}>{children}</div>;
};
export default ListItem;
