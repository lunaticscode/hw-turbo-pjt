import {
  createContext,
  FC,
  ReactNode,
  useState,
  useContext,
  useMemo,
} from "react";
import ListItem from "./components/ListItem";

interface ListContextProps {
  selectedId: string;
  setSelectedId: (id: string) => void;
}

const ListContext = createContext<ListContextProps>({
  selectedId: "",
  setSelectedId: () => {},
});

export const useListContext = () => {
  const context = useContext<ListContextProps>(ListContext);
  if (!context) {
    throw Error("(!)Cannot find 'ListContext'");
  }
  return context;
};
export interface ListCompoundProps {
  Item: typeof ListItem;
}
export interface ListProps {
  children?: ReactNode;
}
const List: FC<ListProps> & ListCompoundProps = (props) => {
  const { children } = props;
  const [selectedId, setSelectedId] = useState<string>("");

  const contextValue: ListContextProps = useMemo(
    () => ({
      selectedId,
      setSelectedId,
    }),
    [selectedId]
  );

  return (
    <ListContext.Provider value={contextValue}>
      <div>{children}</div>
    </ListContext.Provider>
  );
};
export default List;

List.Item = ListItem;
