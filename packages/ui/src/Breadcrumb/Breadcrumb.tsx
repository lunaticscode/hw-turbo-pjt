import { forwardRef, ReactNode, useCallback } from "react";
import ChevronRight from "../icons/ChevronRight";
import { BaseComponentProps } from "../libs/types/common";
import { _addSubCls, addSubCls, makePrefixCls } from "../libs/utils/className";
import "./styles/index.scss";

export type BreadcrumbTypes = "link" | "plain";
export interface BreadcrumbItemProps {
  label: string;
  href?: string;
  disabled?: boolean;
}

interface BreadcrumbProps extends BaseComponentProps {
  items?: Array<BreadcrumbItemProps>;
  seperator?: ReactNode;
  onClickCrumb?: (href?: string) => void;
  type?: BreadcrumbTypes;
}

const breadcrumbClsName = "breadcrumb";

const Breadcrumb = forwardRef<HTMLOListElement, BreadcrumbProps>(
  (props, ref) => {
    const { className, items, onClickCrumb, type = "link", seperator } = props;
    const handleClickCrumb = useCallback(
      (href: string) => {
        if (type === "plain") return;
        onClickCrumb?.(href);
      },
      [onClickCrumb, type]
    );

    const baseCls = className
      ? `${className} ${makePrefixCls(breadcrumbClsName)}`
      : makePrefixCls(breadcrumbClsName);
    const breadcrumbCls = _addSubCls(baseCls, { [type]: true });
    const breadcurmbItemCls = `${breadcrumbCls} breadcrumb-item`;
    const breadcrumbSeperatorCls = `${breadcrumbCls} breadcrumb-sperator`;
    return (
      <ol className={breadcrumbCls} ref={ref}>
        {items?.map(({ label, href, disabled }, index) => {
          return (
            <li
              className={breadcurmbItemCls}
              key={`${label}-${index}`}
              onClick={() => !disabled && handleClickCrumb(href || "")}
            >
              <span>{label}</span>
              <span className={breadcrumbSeperatorCls}>
                {index === items.length - 1 ||
                  (!(items.length <= 1) && (seperator || <ChevronRight />))}
              </span>
            </li>
          );
        })}
      </ol>
    );
  }
);

export default Breadcrumb;
Breadcrumb.displayName = "Breadcrumb";
