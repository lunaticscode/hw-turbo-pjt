import { PropsWithChildren } from "react";

export interface BaseComponentProps extends PropsWithChildren {
  className?: string;
  /* 부모 컴포넌트의 className */
  parentCls?: string;
}
