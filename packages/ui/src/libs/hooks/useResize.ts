import { useEffect, useState, useCallback, useDeferredValue } from "react";

type WindowSize = { width: number; height: number };
const useResize = () => {
  const [windowSize, setWindowSize] = useState<WindowSize>({
    width: window.innerWidth,
    height: window.innerHeight,
  });
  const dfferdWindowSize = useDeferredValue(windowSize);

  const handleResizeEvent = useCallback(() => {
    setWindowSize((prev) => ({
      ...prev,
      width: window.innerWidth,
      height: window.innerHeight,
    }));
  }, []);

  useEffect(() => {
    window.addEventListener("resize", handleResizeEvent);
    return () => window.removeEventListener("resize", handleResizeEvent);
  }, [handleResizeEvent]);
  return dfferdWindowSize;
};

export default useResize;
