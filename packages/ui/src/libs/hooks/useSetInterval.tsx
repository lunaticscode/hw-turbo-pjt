import { useEffect } from "react";

const useSetInterval = <T,>(callback: T, time: number, pause: boolean) => {
  useEffect(() => {
    if (pause) return;
    const interval = setInterval(() => callback, time);
    return () => {
      clearInterval(interval);
    };
  }, []);
  return;
};
export default useSetInterval;
