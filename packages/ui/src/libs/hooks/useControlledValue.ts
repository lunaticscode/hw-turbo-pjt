import { useState, useRef, useCallback, useEffect } from "react";

const useControlledValue = <T>(
  controlledValue: T, // 'value' prop
  defaultValue: T // 'defaultValue' prop
): [T, (value: T) => void] => {
  const { current: isControlled } = useRef<boolean>(
    controlledValue !== undefined // 사용자 측에서 'value' prop 선언 여부에 따라 판단
  );

  const [uncontrolledValue, setUncontrolledValue] = useState<T>(defaultValue);

  const value = isControlled ? controlledValue : uncontrolledValue;
  const setValue = useCallback(
    //* controlled 상태일 경우, 사용자가 onChange 등의 이벤트로 주입한 결과를 그대로 받아서 사용.
    (newValue: T) => {
      if (!isControlled) {
        return setUncontrolledValue(newValue);
      }
    },
    [isControlled]
  );

  useEffect(() => {
    if (uncontrolledValue && isControlled) {
      console.error(
        `비제어 상태인 컴포넌트를 제어하려고 합니다. \nuncontrolledValue: ${uncontrolledValue}, controlledValue: ${controlledValue}\nhttps://ko.legacy.reactjs.org/docs/uncontrolled-components.html#gatsby-focus-wrapper
        `
      );
    }
  }, [isControlled, uncontrolledValue, controlledValue]);

  return [value, setValue];
};

export default useControlledValue;
