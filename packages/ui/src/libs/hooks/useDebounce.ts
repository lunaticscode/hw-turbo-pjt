import { useEffect, useState } from "react";

type UseDebounceProps<T> = {
  value: T;
  delay: number;
};
const useDebounce = <T>({ value, delay }: UseDebounceProps<T>) => {
  const [debounceValue, setDebounceValue] = useState(value);

  useEffect(() => {
    const timeoutHandler = setTimeout(() => setDebounceValue(value), delay);
    return () => clearTimeout(timeoutHandler);
  }, [value, delay]);

  return debounceValue;
};
export default useDebounce;
