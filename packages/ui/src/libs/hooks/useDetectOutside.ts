import { RefObject, useCallback, useEffect, useRef, useState } from "react";

const useDetectOutside = <T extends HTMLElement>(trigger?: RefObject<any>) => {
  const [isOutside, setIsOutside] = useState<boolean | undefined>(undefined);
  const ref = useRef<T>(null);

  const onClick = useCallback(
    ({ target }: MouseEvent) => {
      if ((trigger?.current as Node).contains(target as Node)) {
        setIsOutside(false);
      } else {
        const isContains = ref?.current?.contains(target as Node);
        setIsOutside(!isContains);
      }
    },
    [trigger]
  );

  useEffect(() => {
    window.addEventListener("click", onClick, { capture: true });
    return () => {
      window.removeEventListener("click", onClick, { capture: true });
    };
  }, [onClick]);

  return { ref, isOutside };
};
export default useDetectOutside;
