import { prefixCls } from "./const";

export const makePrefixCls = (cls?: string | { [key: string]: boolean }) => {
  if (!cls) return prefixCls;
  if (typeof cls === "string") {
    return `${prefixCls}__${cls}`;
  }
  // if (typeof cls === "object" && Object.keys(cls).length) {
  //   let subCls = "";
  //   for (const key in cls) {
  //     if (cls[key]) {
  //       subCls = `${subCls}-${key}`;
  //     }
  //   }
  //   return `${prefixCls}-${subCls}`;
  // }
};

export const addSubCls = (
  rootCls?: string,
  addCls?: { [key: string]: boolean | undefined }
) => {
  if (!rootCls) return;
  let cls = rootCls;
  for (const key in addCls) {
    if (addCls[key]) {
      cls = `${cls}--${key}`;
    }
  }
  return cls;
};

export const _addSubCls = (
  rootCls?: string,
  addCls?: { [key: string]: boolean | undefined }
) => {
  if (!rootCls) return;
  let cls = rootCls;
  for (const key in addCls) {
    if (addCls[key]) {
      cls = `${cls} ${key}`;
    }
  }
  return cls;
};
