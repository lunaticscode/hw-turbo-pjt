import { useMemo, useState } from "react";

interface UsePaginatorProps {
  defaultPage?: number;
  totalCount?: number;
  divideCount?: number;
}

interface UsePaginatorRO {
  page: number;
  isFirst: boolean;
  isLast: boolean;
  handleClickPrev: () => void;
  handleClickNext: () => void;
  handleMoveTo: (page: number) => void;
}

const usePaginator = ({
  defaultPage = 1,
  totalCount = 0,
  divideCount = 10,
}: UsePaginatorProps): UsePaginatorRO => {
  const [page, setPage] = useState<number>(defaultPage);

  const handleClickPrev = () => {};
  const handleClickNext = () => {};
  const handleMoveTo = () => {};

  const isFirst = useMemo(() => true, []);
  const isLast = useMemo(() => true, []);

  return {
    page,
    isFirst,
    isLast,
    handleClickPrev,
    handleClickNext,
    handleMoveTo,
  };
};
export default usePaginator;
