import { forwardRef } from "react";
import { BaseComponentProps } from "../../libs/types/common";

interface PaginatorProps extends BaseComponentProps {}
const Paginator = forwardRef<HTMLDivElement, PaginatorProps>((props, ref) => {
  return <div ref={ref}></div>;
});
export default Paginator;
Paginator.displayName = "Paginator";
