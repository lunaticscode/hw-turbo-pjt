import { forwardRef } from "react";
import { BaseComponentProps } from "../libs/types/common";
interface TableProps extends BaseComponentProps {}
// 1) 로우 클릭 이벤트
// 2) 컬럼 구성 요소(ReactNode) 사용자에게 위임
// 3) Table.Paginator 컴파운드 형식으로 제공
// 4) 정렬 기능
// 5) 컬럼 숨기기 imperativeHandle로 제공
const Table = forwardRef<HTMLDivElement, TableProps>((props, ref) => {
  return <div ref={ref}></div>;
});
export default Table;
Table.displayName = "Table";
