import {
  createContext,
  CSSProperties,
  Dispatch,
  forwardRef,
  ReactElement,
  ReactNode,
  useContext,
  useMemo,
  useState,
} from "react";
import { makePrefixCls } from "../libs/utils/className";
import useControlledValue from "../libs/hooks/useControlledValue";
import CalendarBody from "./components/CalendarBody";
import CalendarHeader from "./components/CalendarHeader";
import "./styles/index.scss";
export type DateType = Date | null;
export type DateLangType = "ko" | "en";
export type DateSelectType = "years" | "months" | "days";

export interface CalendarProps {
  className?: string;
  style?: CSSProperties;
  onChange?: (date: DateType) => void;
  onSelect?: (date: DateType) => void;
  defaultValue?: DateType;
  value?: DateType;
  lang?: DateLangType;
  format?: "yyyy-MM-dd";
  maxDate?: Date;
  minDate?: Date;
  renderDateCell?: (date: Date) => ReactNode;
  renderHeader?: (
    setSelectType?: Dispatch<React.SetStateAction<DateSelectType>>,
    date?: Date,
    prev?: () => void,
    next?: () => void
  ) => ReactElement;
  disabledDate?: (date: Date) => boolean;
  // role?: HTMLAttributes<HTMLDivElement>["role"];
  role?: "calendar" | "datepicker";
}

interface CalendarContextProps extends CalendarProps {
  setValue: (date: DateType) => void; //* From useControlledValue()
  value: DateType | undefined;
  setSelectType: Dispatch<React.SetStateAction<DateSelectType>>;
  selectType: DateSelectType;
  handleChangeByNav: (date: DateType) => void;
  handleChangeByDateCell: (date: DateType) => void;
  handleChangeByHeader: (date: DateType) => void;
}

const CalendarContext = createContext<CalendarContextProps>({
  setValue: () => {},
  value: new Date(),
  setSelectType: () => {},
  selectType: "days",
  handleChangeByNav: () => {},
  handleChangeByDateCell: () => {},
  handleChangeByHeader: () => {},
});

export const useCalendarContext = () => {
  const context = useContext(CalendarContext);
  if (!context) {
    throw Error("(!)Cannot access CalendarContext.");
  }
  return context;
};

const calendarCls = makePrefixCls("calendar");
const Calendar = forwardRef<HTMLDivElement, CalendarProps>((props, ref) => {
  const {
    className,
    defaultValue,
    value: valueProp,
    format = "yyyy-MM-dd",
    lang = "ko",
    onChange,
    onSelect,
    renderDateCell,
    renderHeader,
    style,
    role = "calendar",
    minDate,
    maxDate,
    disabledDate,
  } = props;

  const [value, setValue] = useControlledValue(valueProp, defaultValue);
  // const [selectedDate, setSelectedDate] = useState<DateType>(
  //   value || new Date()
  // ); // view 제어를 위한 전용 상태

  const calendarBaseCls = useMemo(
    () => (className ? `${className} ${calendarCls}` : calendarCls),
    [className]
  );

  const [selectType, setSelectType] = useState<DateSelectType>("days");

  const handleChangeByDateCell = (date: DateType) => {
    setValue(date);
    //* TODO ::: setSelectedDate 추가
    onChange?.(date);
  };

  const handleChangeByNav = (date: DateType) => {
    setValue(date); // 제거
    //* TODO ::: setSelectedDate 추가
    onChange?.(date); // 제거
  };

  const handleChangeByHeader = (date: DateType) => {
    //* TODO ::: setSelectedDate 추가
    setValue(date); // 제거
  };

  const contextValue = {
    value,
    selectType,
    format,
    lang,
    setValue,
    setSelectType,
    onSelect,
    renderDateCell,
    renderHeader,
    handleChangeByNav,
    handleChangeByDateCell,
    handleChangeByHeader,
    minDate,
    maxDate,
    disabledDate,
  };

  return (
    <div className={calendarBaseCls} style={style} ref={ref} role={role}>
      <CalendarContext.Provider value={contextValue}>
        <CalendarHeader />
        <CalendarBody />
      </CalendarContext.Provider>
    </div>
  );
});

export default Calendar;
Calendar.displayName = "Calendar";
