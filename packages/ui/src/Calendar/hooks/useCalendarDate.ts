import { useMemo } from "react";
import { DateSelectType, useCalendarContext } from "../Calendar";
import {
  add,
  endOfMonth,
  getWeekOfMonth,
  startOfMonth,
  startOfWeek,
  startOfYear,
} from "date-fns";

const getDateList = (selectedDate: Date, selectType: DateSelectType) => {
  const mapSelectTypeToDateList = {
    years: [],
    months: Array.from({ length: 4 }, (_, monthCnt) =>
      Array.from({ length: 3 }, (_, monthIndex) =>
        add(startOfYear(selectedDate), { months: monthIndex + 3 * monthCnt })
      )
    ),
    days: Array.from(
      { length: getWeekOfMonth(endOfMonth(selectedDate)) },
      (_, weekIndex) =>
        Array.from({ length: 7 }, (_, dateIndex) =>
          add(startOfWeek(startOfMonth(selectedDate)), {
            days: dateIndex + weekIndex * 7,
          })
        )
    ),
  };
  return mapSelectTypeToDateList[selectType];
};

const useCalendarDate = () => {
  const { value, selectType = "days" } = useCalendarContext();
  const selectedDate = useMemo(() => value || new Date(), [value]);

  const dateList = useMemo(
    () => getDateList(selectedDate || new Date(), selectType),
    [selectedDate, selectType]
  );

  return { dateList };
};
export default useCalendarDate;
