import { FC, PropsWithChildren, useCallback, useMemo } from "react";
import {
  getDate,
  getMonth,
  getYear,
  isAfter,
  isBefore,
  isSameMonth,
} from "date-fns";
import { DateSelectType, DateType, useCalendarContext } from "../Calendar";
import { addSubCls, makePrefixCls } from "../../libs/utils/className";
import { isSameDay } from "date-fns/esm";

interface CalendarDateCellProps extends PropsWithChildren {
  date: Date;
}

const getMemorizedRule = (selectType: DateSelectType, date: Date) => {
  if (selectType === "days") return getMonth(date);
  if (selectType === "months") return getYear(date);
};

const isDisabledDate = (
  minDate?: Date,
  maxDate?: Date,
  disabledDate?: (date: Date) => boolean,
  date?: Date
) => {
  if (date && maxDate && isAfter(date, maxDate)) return true;
  if (date && minDate && isBefore(date, minDate)) return true;
  if (date && disabledDate?.(date)) return true;
  return false;
};

const CalendarDateCell: FC<CalendarDateCellProps> = ({
  children,
  ...props
}) => {
  const {
    renderDateCell,
    selectType,
    handleChangeByDateCell,
    setSelectType,
    minDate,
    maxDate,
    disabledDate,
    value,
  } = useCalendarContext();
  const { date } = props;
  const selectedDate = useMemo(() => value || new Date(), [value]);

  const renderedDateCell = useMemo(
    () => renderDateCell?.(date) || null,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [renderDateCell, getMemorizedRule(selectType, date)]
  );

  const isDisabled = useMemo(
    () => isDisabledDate(minDate, maxDate, disabledDate, date),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [minDate, maxDate, disabledDate, getMemorizedRule(selectType, date)]
  );
  const calendarDeteCellCls = makePrefixCls("calendar_date_cell");
  const dateCellCls = useMemo(
    () =>
      addSubCls(calendarDeteCellCls, {
        current_month: isSameMonth(date, selectedDate),
        selected:
          selectType === "days"
            ? isSameDay(date, selectedDate)
            : isSameMonth(date, selectedDate),
        disabled: isDisabled,
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isDisabled, getMemorizedRule(selectType, date), selectedDate]
  );

  const handleClickDate = useCallback(
    (date: DateType) => {
      if (isDisabled) return;
      if (selectType === "months") {
        setSelectType("days");
      }
      handleChangeByDateCell(date);
    },
    [handleChangeByDateCell, setSelectType, selectType, isDisabled]
  );
  return (
    <div className={dateCellCls} onClick={() => handleClickDate(date)}>
      {selectType === "days" && getDate(date)}
      {selectType === "months" && getMonth(date) + 1}
      {renderedDateCell}
    </div>
  );
};
export default CalendarDateCell;
