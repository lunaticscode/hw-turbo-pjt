import { FC, useMemo } from "react";
import { makePrefixCls } from "../../libs/utils/className";
import { useCalendarContext } from "../Calendar";
import useCalendarDate from "../hooks/useCalendarDate";
import CalendarDateCell from "./CalendarDateCell";
interface CalendarBodyProps {}

const calendarBodyCls = makePrefixCls("calendar_body");

const CalendarBody: FC<CalendarBodyProps> = () => {
  const { dateList } = useCalendarDate();
  const { selectType } = useCalendarContext();
  const dateRowCls = useMemo(() => {
    if (selectType === "months")
      return makePrefixCls("calendar_date_row_month");
    return makePrefixCls("calendar_date_row");
  }, [selectType]);
  return (
    <div className={calendarBodyCls}>
      {dateList?.map((line, lineIndex) => (
        <div key={`date_row_${lineIndex}`} className={dateRowCls}>
          {line.map((date, dateIndex) => (
            <CalendarDateCell key={`date_cell_${dateIndex}`} date={date} />
          ))}
        </div>
      ))}
      <br />
    </div>
  );
};
export default CalendarBody;
