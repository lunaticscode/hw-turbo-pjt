import { add, format as dateFormat } from "date-fns";
import { FC, ReactElement, ReactNode, useCallback, useMemo } from "react";
import ChevronLeft from "../../icons/ChevronLeft";
import ChevronRight from "../../icons/ChevronRight";
import { makePrefixCls } from "../../libs/utils/className";
import { useCalendarContext } from "../Calendar";
interface CalendarHeaderProps {}
const calendarHeaderCls = makePrefixCls("calendar_header");

const CalendarHeader: FC<CalendarHeaderProps> = () => {
  const {
    value,
    setSelectType,
    selectType,
    format,
    renderHeader,
    handleChangeByHeader,
    handleChangeByNav,
  } = useCalendarContext();

  const selectedDate = useMemo(() => value || new Date(), [value]);
  const addType = useMemo(() => {
    if (selectType === "days") {
      return "months";
    }
    if (selectType === "months") {
      return "years";
    }
  }, [selectType]);

  const handleClickNav = useCallback(
    (nav: number) => {
      const newDate = add(selectedDate, {
        [addType as "months" | "years"]: nav,
      });
      handleChangeByNav(newDate);
    },
    [selectedDate, handleChangeByNav, addType]
  );

  const prevTrigger = useCallback(() => handleClickNav(-1), [handleClickNav]);
  const nextTrigger = useCallback(() => handleClickNav(1), [handleClickNav]);

  const controlledHeader = useMemo(
    () => renderHeader?.(setSelectType, selectedDate, prevTrigger, nextTrigger),
    [selectedDate, setSelectType, prevTrigger, nextTrigger, renderHeader]
  );

  const headerFormatDate = useMemo(() => {
    if (selectType === "days") {
      return dateFormat(selectedDate, format as string);
    }
    if (selectType === "months") {
      return dateFormat(selectedDate, "yyyy-MM");
    }
  }, [selectedDate, format, selectType]);

  const handleClickHeader = () => {
    if (selectType === "days") {
      setSelectType("months");
    }
    // if (selectType === "months") { setSelectType("years"); }
    handleChangeByHeader(selectedDate);
  };

  if (controlledHeader) {
    return controlledHeader;
  }
  return (
    <div className={calendarHeaderCls}>
      <ChevronLeft
        style={{ width: "20px" }}
        className={`${calendarHeaderCls}_nav_button`}
        onClick={() => handleClickNav(-1)}
      />
      <div
        className={`${calendarHeaderCls}_formated_date`}
        onClick={handleClickHeader}
      >
        {headerFormatDate}
      </div>
      <ChevronRight
        style={{ width: "20px" }}
        className={`${calendarHeaderCls}_nav_button`}
        onClick={() => handleClickNav(1)}
      />
    </div>
  );
};
export default CalendarHeader;
