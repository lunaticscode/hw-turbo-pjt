import { MouseEventHandler } from "react";

const REVOKE_EVENT = {
  Page: {
    load: false,
  },
  Button: {
    click: false,
  },
};

interface PageTrackerProps {
  load?: () => void;
}

interface ButtonTrackerProps {
  click?: MouseEventHandler<HTMLButtonElement>;
}

type ComponentEvents = {
  Page: PageTrackerProps;
  Button: ButtonTrackerProps;
};

const getPathname = () => {
  if (window && typeof window === "object") {
    return window.location.href;
  }
  console.log("(!) Cannot find 'window' object.");
  return null;
};

export const Events: ComponentEvents = {
  Page: {
    load: () => {
      if (REVOKE_EVENT.Page.load) return;
      console.log(getPathname());
    },
  },
  Button: {
    click: (e) => {
      if (REVOKE_EVENT.Button.click) return;
      console.log(getPathname());
    },
  },
};
