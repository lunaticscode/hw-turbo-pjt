import { forwardRef, useEffect, useContext, useMemo, useId } from "react";
import { BaseComponentProps } from "../libs/types/common";
import useControlledValue from "../libs/hooks/useControlledValue";
import { CheckboxValue } from "./Checkbox";
import { CheckboxGroup2Context } from "./CheckboxGroup2";
interface Checkbox2Props extends BaseComponentProps {
  checked?: boolean;
  defaultChecked?: boolean;
  value: CheckboxValue;
  onChange?: (value: CheckboxValue, checked: boolean) => void;
  disabled?: boolean;
}
const Checkbox2 = forwardRef<HTMLInputElement, Checkbox2Props>((props, ref) => {
  const id = useId();
  const {
    children,
    checked: checkedProp,
    defaultChecked: defaultCheckedProp,
    value: valueProp,
    disabled: disabledProp,
    onChange,
  } = props;

  const {
    value: groupValue,
    handleChangeValue: handleChangeGroupValue,
    parentChecked: groupParentChecked,
  } = useContext(CheckboxGroup2Context);

  const [checked, setChecked] = useControlledValue(
    checkedProp,
    defaultCheckedProp
  );

  const isChecked = useMemo(() => {
    if (groupValue) {
      console.log("groupvalue", groupValue);
      return groupValue.includes(valueProp);
    }
    return groupParentChecked ?? checked;
  }, [groupValue, valueProp, checked, groupParentChecked]);

  useEffect(() => {}, [groupParentChecked, setChecked]);

  const handleChangeChecked = (value: CheckboxValue, checked: boolean) => {
    setChecked(checked);
    if (checked) {
      if (groupValue) {
        const newGroupValue = groupValue.includes(value)
          ? groupValue
          : [...groupValue, value];
        handleChangeGroupValue?.(newGroupValue);
      }
    } else {
      if (groupValue) {
        const newGroupValue = groupValue.filter((val) => val !== value);
        handleChangeGroupValue?.(newGroupValue);
      }
    }
    onChange?.(value, checked);
  };

  useEffect(() => {
    if (groupParentChecked) {
      const newGroupValue = groupValue?.includes(valueProp)
        ? groupValue
        : [...(groupValue || []), valueProp];
      handleChangeGroupValue?.(newGroupValue);
      onChange?.(valueProp, true);
    } else {
      const newGroupValue = groupValue?.filter((val) => val !== valueProp);
      handleChangeGroupValue?.(newGroupValue || []);
      onChange?.(valueProp, false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [groupParentChecked]);

  return (
    <>
      <input
        id={id}
        type="checkbox"
        checked={isChecked}
        disabled={disabledProp}
        onChange={(e) => handleChangeChecked(valueProp, e.target.checked)}
      />
      <label htmlFor={id}>{children}</label>
    </>
  );
});

export default Checkbox2;
Checkbox2.displayName = "Checkbox2";
