import {
  ChangeEvent,
  forwardRef,
  RefObject,
  useCallback,
  useId,
  useImperativeHandle,
  useMemo,
  useRef,
} from "react";

import { BaseComponentProps } from "../libs/types/common";
import useControlledValue from "../libs/hooks/useControlledValue";
import { CheckboxGroupRef, useCheckboxGroupContext } from "./CheckboxGroup";

export type CheckboxValue = string;

export interface CheckboxProps extends BaseComponentProps {
  onChange?: ({
    value,
    checked,
  }: {
    value: CheckboxValue;
    checked: boolean;
  }) => void;
  disabled?: boolean;
  checked?: boolean;
  defaultChecked?: boolean;
  value: CheckboxValue;
  childs?: Array<RefObject<CheckboxRef>>;
  childGroups?: Array<RefObject<CheckboxGroupRef>>;
}

export interface CheckboxRef extends RefObject<HTMLInputElement> {
  check: () => void;
  uncheck: () => void;
  groupCheck: () => { value: CheckboxValue; checked: boolean };
  groupUncheck: () => { value: CheckboxValue; checked: boolean };
}

const Checkbox = forwardRef<CheckboxRef, CheckboxProps>((props, ref) => {
  const id = useId();

  const {
    // setChangedGroupValue,
    disabled: groupDisabled,
    value: groupValue,
    setValue: setGroupValue,
  } = useCheckboxGroupContext();

  const {
    children,
    onChange,
    value,
    // disabled: disabledProp,
    disabled,
    checked: checkedProp,
    defaultChecked: defaultCheckedProp = false,
    childs,
    childGroups,
  } = props;

  const [checkedValue, setChecked] = useControlledValue<boolean | undefined>(
    checkedProp,
    defaultCheckedProp
  );

  const checked = useMemo(
    () => groupValue?.includes(value) ?? checkedValue,
    [groupValue, value, checkedValue]
  );
  console.log(value, checked);

  // const disabled = useMemo(
  //   () => disabledProp ?? groupDisabled ?? false,
  //   [disabledProp, groupDisabled]
  // );

  const inputRef = useRef<HTMLInputElement>(null);

  const handleChange = useCallback(
    (checked: boolean) => {
      if (disabled) return;

      setChecked(checked);
      onChange?.({ value, checked });
      if (checked) {
        setGroupValue?.([...(groupValue || []), value]);
      } else {
        setGroupValue?.(groupValue?.filter((prev) => prev !== value));
      }

      // childs :: Array<Checkbox>
      if (childs) {
        childs.forEach((child) => {
          if (checked) {
            child.current?.check();
          } else {
            child.current?.uncheck();
          }
        });
      }

      // childGroups :: Array<CheckboxGroup>
      if (childGroups) {
        // console.log(childGroups);
        if (checked) {
          childGroups.forEach((childGroup) => childGroup.current?.allCheck());
        } else {
          childGroups.forEach((childGroup) => childGroup.current?.allUncheck());
        }
      }
    },
    [
      onChange,
      disabled,
      value,
      setChecked,
      childs,
      childGroups,
      setGroupValue,
      groupValue,
    ]
  );

  // const check = useCallback(() => {
  //   if (disabled || checked) return;
  //   setChecked(true);
  //   onChange?.({ value, checked: true });
  // }, [disabled, checked, value, setChecked, onChange]);
  const check = useCallback(() => {
    if (disabled || checked) return;
    handleChange(true);
  }, [disabled, checked, handleChange]);

  // const uncheck = useCallback(() => {
  //   if (disabled || !checked) return;
  //   setChecked(false);
  //   onChange?.({ value, checked: false });
  // }, [disabled, checked, value, setChecked, onChange]);

  const uncheck = useCallback(() => {
    if (disabled || !checked) return;
    handleChange(false);
  }, [disabled, checked, handleChange]);

  // [FIX] parentCheck, parentUncheck
  // :: imperativeHandle --> context state 순서로 진행되고 있어서, groupDisabled 를 못써먹음.
  // :: 이미 해당 이벤트를 호출한다는 것 자체가 groupDisabled를 직접 컨트롤 하기 때문에 여기서 groupDisabled를 체크할 필요 없을 수도...?

  // const parentCheck = () => {
  //   if (disabledProp) return { value, checked: checked ?? false };
  //   setChecked(true);
  //   return { value, checked: true };
  // };

  const parentCheck = () => {
    if (disabled) return { value, checked: checked ?? false };
    handleChange(true);
    return { value, checked: true };
  };

  const parentUncheck = () => {
    if (disabled) return { value, checked: checked ?? false };
    handleChange(false);
    return { value, checked: false };
  };

  useImperativeHandle(ref, () => ({
    current: inputRef.current,
    check,
    uncheck,
    groupCheck: parentCheck,
    groupUncheck: parentUncheck,
  }));

  return (
    <>
      <input
        ref={inputRef}
        id={id}
        onChange={(e) => handleChange(e.target.checked)}
        disabled={disabled}
        // checked={checked}
        checked={checked}
        value={disabled ? undefined : value}
        type="checkbox"
      />
      <label htmlFor={id}>{children}</label>
    </>
  );
});

Checkbox.displayName = "Checkbox";
export default Checkbox;
