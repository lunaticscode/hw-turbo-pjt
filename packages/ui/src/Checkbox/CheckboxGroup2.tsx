import { createContext, forwardRef } from "react";
import { BaseComponentProps } from "../libs/types/common";
import useControlledValue from "../libs/hooks/useControlledValue";
import { CheckboxValue } from "./Checkbox";

interface CheckboxGroup2ContextProps
  extends Pick<CheckboxGroup2Props, "parentChecked"> {
  handleChangeValue?: (value: CheckboxValue[]) => void;
  value?: CheckboxValue[];
}
export const CheckboxGroup2Context = createContext<CheckboxGroup2ContextProps>({
  value: undefined,
  handleChangeValue: () => {},
  parentChecked: undefined,
});

interface CheckboxGroup2Props extends BaseComponentProps {
  parentChecked?: boolean;
  value?: CheckboxValue[];
  defaultValue?: CheckboxValue[];
  onChange?: (value: CheckboxValue[]) => void;
  disabled?: boolean;
}

const CheckboxGroup2 = forwardRef<HTMLDivElement, CheckboxGroup2Props>(
  (props, ref) => {
    const {
      value: valueProp,
      defaultValue: defaultValueProp,
      onChange,
      disabled: disabledProp,
      parentChecked,
      children,
    } = props;

    const [value, setValue] = useControlledValue(valueProp, defaultValueProp);

    const handleChangeValue = (value: CheckboxValue[]) => {
      console.log("handleChangeGroupValue", value);
      setValue(value);
      onChange?.(value);
    };

    const contextValue: CheckboxGroup2ContextProps = {
      value,
      handleChangeValue,
      parentChecked,
    };

    return (
      <CheckboxGroup2Context.Provider value={contextValue}>
        <div ref={ref}>{children}</div>
      </CheckboxGroup2Context.Provider>
    );
  }
);
export default CheckboxGroup2;
CheckboxGroup2.displayName = "CheckboxGroup2";
