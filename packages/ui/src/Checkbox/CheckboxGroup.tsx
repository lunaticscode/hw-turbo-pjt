import {
  Children,
  cloneElement,
  createContext,
  Dispatch,
  forwardRef,
  RefObject,
  SetStateAction,
  useCallback,
  useContext,
  useImperativeHandle,
  useMemo,
  useRef,
  useState,
} from "react";
import { BaseComponentProps } from "../libs/types/common";
import useControlledValue from "../libs/hooks/useControlledValue";
import Checkbox, { CheckboxRef, CheckboxValue } from "./Checkbox";

interface CheckboxGroupProps extends BaseComponentProps {
  onChange?: (value: CheckboxValue[]) => void;
  value?: CheckboxValue[];
  defaultValue?: CheckboxValue[];
  disabled?: boolean;
  orientation?: "vertical" | "horizon";
}

interface CheckboxGroupContextProps {
  setChangedGroupValue?: (checkbox: {
    value: CheckboxValue;
    checked: boolean;
  }) => void;
  groupValue?: Array<CheckboxValue>;
  disabled?: boolean;
  value?: Array<CheckboxValue>;
  setValue?: (value: CheckboxValue[] | undefined) => void;
}

export const CheckboxGroupContext = createContext<CheckboxGroupContextProps>({
  setChangedGroupValue: () => {},
  groupValue: [],
  disabled: false,
  value: [],
  setValue: () => {},
});

export const useCheckboxGroupContext = () => {
  const context = useContext(CheckboxGroupContext);
  if (!context) {
    throw new Error("(!)CheckboxGroupContext 를 호출할 수 없는 범위입니다.");
  }
  return context;
};

export interface CheckboxGroupRef extends RefObject<HTMLDivElement> {
  allCheck: () => void;
  allUncheck: () => void;
}

const CheckboxGroup = forwardRef<CheckboxGroupRef, CheckboxGroupProps>(
  (props, ref) => {
    const {
      children,
      onChange,
      value: valueProp,
      defaultValue: defaultValueProp,
      disabled,
      orientation = "vertical",
    } = props;

    const checkboxGroupRef = useRef<HTMLDivElement>(null);
    const innerRefs = useRef<CheckboxRef[]>([]);
    const [value, setValue] = useControlledValue<CheckboxValue[] | undefined>(
      valueProp,
      defaultValueProp
    );
    const [groupChecked, setGroupChecked] = useState();

    const childrenElems = useMemo(
      () => Children.toArray(children) as JSX.Element[],
      [children]
    );

    const checkboxElems = useMemo(
      () => childrenElems.filter((child) => child.type === Checkbox),
      [childrenElems]
    );

    const allCheck = () => {
      // setDisabled(false);
      const resultList = Array.from(innerRefs.current)
        .map?.((ref) => ref.groupCheck())
        .filter?.((check) => check.checked)
        .map((check) => check.value);
      setValue(resultList);
      onChange?.(resultList);
    };

    const allUncheck = () => {
      // setDisabled(true);
      const resultList = Array.from(innerRefs.current)
        .map?.((ref) => ref.groupUncheck())
        .filter?.((check) => check.checked)
        .map((check) => check.value);
      setValue(resultList);
      onChange?.(resultList);
    };

    useImperativeHandle(ref, () => ({
      current: checkboxGroupRef.current,
      allCheck,
      allUncheck,
    }));

    const handleChangeChilds = useCallback(
      (checkbox: { value: CheckboxValue; checked: boolean }) => {
        let newValue: Array<CheckboxValue> = [];
        if (checkbox.checked) {
          if (value?.includes(checkbox.value)) newValue = value;
          else {
            newValue = [...(value || []), checkbox.value];
          }
        } else {
          newValue = value?.filter((value) => value !== checkbox.value) || [];
        }
        setValue(newValue);
        onChange?.(newValue);
      },
      [onChange, setValue, value]
    );

    // const setChangedGroupValue = (checkbox: {
    //   value: CheckboxValue;
    //   checked: boolean;
    // }) => {
    //   let newValue: Array<CheckboxValue> = [];
    //   if (checkbox.checked) {
    //     if (value?.includes(checkbox.value)) newValue = value;
    //     else {
    //       newValue = [...(value || []), checkbox.value];
    //     }
    //   } else {
    //     newValue = value?.filter((value) => value !== checkbox.value) || [];
    //   }
    //   setValue(newValue);
    //   onChange?.(newValue);
    // };

    const contextValue: CheckboxGroupContextProps = {
      // setChangedGroupValue,
      disabled,
      value,
      setValue,
    };

    return (
      <CheckboxGroupContext.Provider value={contextValue}>
        <div ref={checkboxGroupRef} role={"group"}>
          {checkboxElems.map((elem, index) => {
            return cloneElement(elem, {
              ref: (checkboxRef: CheckboxRef) =>
                (innerRefs.current[index] = checkboxRef),
              // onChange: handleChangeChilds,
              // checked: value?.includes(elem.props.value),
              // disabled: elem.props.disabled ?? disabled ?? false,
              ...elem.props,
            });
          })}
        </div>
      </CheckboxGroupContext.Provider>
    );
  }
);

CheckboxGroup.displayName = "CheckboxGroup";
export default CheckboxGroup;
