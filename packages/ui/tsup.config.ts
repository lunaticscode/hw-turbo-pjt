import fs from "fs";
import path from "path";
import { defineConfig } from "tsup";
import { sassPlugin } from "esbuild-sass-plugin";
import postcss from "postcss";
import autoPrefixer from "autoprefixer";
import postcsssPresetEnv from "postcss-preset-env";

const createPacakgeJson = () => {
  const currentPath = process.cwd();
  const targetPath = path.resolve(currentPath, "build");
  let rootPackageJsonFile;
  try {
    rootPackageJsonFile = fs.readFileSync(
      path.resolve(currentPath, "package.json"),
      {
        encoding: "utf-8",
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
  if (!rootPackageJsonFile) {
    console.log({ rootPackageJsonFile }, "Invalid package.json file.");
  }
  const { devDependencies, dependencies, scripts, ...rest } =
    JSON.parse(rootPackageJsonFile);
  const newPackageJson = {
    ...rest,
    name: "hw-rui",
    main: "./index.js",
    types: "./index.d.ts",
    module: "./esm/index.js",
    private: false,
  };
  try {
    console.log(`Create new package.json to '${targetPath}'....`);
    fs.writeFileSync(
      path.resolve(targetPath, "package.json"),
      JSON.stringify(newPackageJson, null, 2),
      { encoding: "utf-8" }
    );
  } catch (err) {
    console.log(err);
    console.log(`Failed create new package.json file....`);
    throw err;
  }
};

export default defineConfig((options) => {
  return {
    esbuildPlugins: [
      sassPlugin({
        loadPaths: ["@import", "@mixin", "@include"],
        async transform(source, resolveDir, filePath) {
          const { css } = await postcss([
            autoPrefixer,
            postcsssPresetEnv({ stage: 0 }),
          ]).process(source);
          // console.log({ filePath });
          // console.log({ resolveDir });
          // console.log({ css });
          return css;
        },
        cssImports: true,
        filter: /\.scss$/,
      }),
    ],

    entry: ["src/**/*", "!src/styles/**/*", "!src/**/styles/**/*"],
    splitting: false,
    clean: true,
    format: ["cjs", "esm"],
    sourcemap: false,
    bundle: true,
    dts: {
      resolve: ["src/**/*", "!src/styles/**/*", "!src/**/styles/**/*"],
    },
    legacyOutput: true, // esm 폴더 생성
    target: "es5", // @swc/core 필요
    outDir: "build",
    minify: false,
    platform: "browser",
    external: ["react"],
    async onSuccess() {
      // package.json 생성
      createPacakgeJson();
    },
  };
});
