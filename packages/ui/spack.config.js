module.exports = {
  entry: {
    ui: __dirname + "/src/index.ts",
  },
  output: {
    path: __dirname + "/lib",
  },
};
