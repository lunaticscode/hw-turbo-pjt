const rollupResolve = require("@rollup/plugin-node-resolve");
const rollupBabel = require("@rollup/plugin-babel");
const pkgJsonFile = require("./package.json");
const rollupTypescript = require("rollup-plugin-typescript2");
const rollupScss = require("rollup-plugin-scss");
const rollupCjs = require("@rollup/plugin-commonjs");
// const rollupPostcss = require("rollup-plugin-postcss");
// const rollupPostcssPrefixer = require("postcss-prefixer");
const rollupPeerDeps = require("rollup-plugin-peer-deps-external");
const react = require("react");

const extensions = [".js", ".jsx", ".tsx", ".ts"];

module.exports = {
  input: pkgJsonFile.main,
  output: [
    {
      file: "dist/index.js",
      format: "cjs",
      sourcemap: false,
      // preserveModules: true,
      globals: {
        "react/jsx-runtime": "jsxRuntime",
        "react-dom/client": "ReactDOM",
        react: "React",
      },
    },
  ],

  plugins: [
    rollupCjs({
      include: /node_modules/,
      namedExports: { react: Object.keys(react) },
    }),
    rollupTypescript({
      useTsconfigDeclarationDir: true,
      tsconfing: "./tsconfig.json",
    }),
    rollupResolve({ extensions }),
    rollupScss({
      fileName: "index.css",
    }),
    rollupBabel({
      babelHelpers: "bundled",
      presets: [
        "@babel/preset-env",
        ["@babel/preset-react", { runtime: "automatic" }],
        "@babel/preset-typescript",
      ],
      include: ["src/**/*"],
      exclude: "node_modules/**",
      extensions,
    }),
    rollupPeerDeps(),
  ],

  // external: ["react", "react-dom", "react/jsx-runtime"],
};
