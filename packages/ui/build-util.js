const path = require("path");
const fs = require("fs");

const createPacakgeJson = () => {
  const currentPath = process.cwd();
  const targetPath = path.resolve(currentPath, "dist");
  let rootPackageJsonFile;
  try {
    rootPackageJsonFile = fs.readFileSync(
      path.resolve(currentPath, "package.json"),
      {
        encoding: "utf-8",
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
  if (!rootPackageJsonFile) {
    console.log({ rootPackageJsonFile }, "Invalid package.json file.");
  }
  const { devDependencies, dependencies, scripts, ...rest } =
    JSON.parse(rootPackageJsonFile);
  const newPackageJson = {
    ...rest,
    name: "hw-rui",
    main: "./index.js",
    types: "./index.d.ts",
    module: "./esm/index.js",
    private: false,
  };
  try {
    console.log(`Create new package.json to '${targetPath}'....`);
    fs.writeFileSync(
      path.resolve(targetPath, "package.json"),
      JSON.stringify(newPackageJson),
      { encoding: "utf-8" }
    );
  } catch (err) {
    console.log(err);
    console.log(`Failed create new package.json file....`);
    throw err;
  }
};

module.exports = {
  createPacakgeJson,
};
