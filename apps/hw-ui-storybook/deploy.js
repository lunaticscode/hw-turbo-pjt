/* eslint-disable turbo/no-undeclared-env-vars */
import mime from "mime";
import { config } from "dotenv";
import path from "path";
import fs from "fs";
import AWS from "aws-sdk";
config({ path: ".env.deploy" });

const BUILD_FOLDER = "storybook-static";

const nowPath = process.cwd();
const s3 = new AWS.S3({
  region: "ap-northeast-2",
  credentials: {
    accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY,
  },
});

const deployToS3 = () => {
  const readFiles = (_path) => {
    fs.readdir(_path, async (err, files) => {
      if (err) {
        console.log({ err });
        throw err;
      }
      if (files && files.length) {
        files.forEach((file) => {
          const fsStat = fs.statSync(path.resolve(_path, file));
          if (fsStat.isDirectory()) {
            readFiles(path.resolve(_path, file));
          } else {
            const fileAbsPath = path.resolve(_path, file);
            const s3UploadPath = path.relative(
              path.resolve(nowPath, BUILD_FOLDER),
              path.resolve(_path, file)
            );
            console.log({ fileAbsPath, s3UploadPath });
            uploadToS3(fileAbsPath, s3UploadPath);
          }
        });
      }
    });
  };

  const uploadToS3 = (fileAbsPath, s3UploadPath) => {
    console.log(fs.readFileSync(fileAbsPath));
    s3.upload(
      {
        ACL: "public-read",
        ContentType: mime.getType(fileAbsPath),
        Bucket: process.env.AWS_S3_BUCEKT_NAME,
        Key: s3UploadPath,
        Body: fs.readFileSync(fileAbsPath),
      },

      (err, data) => {
        if (err) {
          console.log("error", s3UploadPath);
          console.log(err);
          throw err;
        }
        console.log('"success"', s3UploadPath, data);
      }
    );
  };
  readFiles(path.resolve(nowPath, BUILD_FOLDER));
};
deployToS3();
