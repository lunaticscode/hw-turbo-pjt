import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { Button, Page } from "ui";

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <Page>
        <Button>TestButton</Button>
      </Page>
    </>
  );
}

export default App;
