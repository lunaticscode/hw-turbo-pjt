import React from "react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Accordian } from "ui";
import SandpackEditor from "../common/SandpackEditor";

export default {
  title: "Example/Accordian",
  component: Accordian,
  tags: ["docs"],
} as StoryObj<typeof Accordian>;

const Template: StoryFn<typeof Accordian> = (args) => {
  return (
    <>
      <Accordian {...args}>
        <Accordian.Title>Title</Accordian.Title>
        <Accordian.Content>Content-1</Accordian.Content>
        <Accordian.Content>Content-2</Accordian.Content>
      </Accordian>
      <SandpackEditor
        code={`import { Accordian } from 'hw-rui';
const App = () => {
    return (
      <Accordian>
        <Accordian.Title>Title</Accordian.Title>
        <Accordian.Content>Content-1</Accordian.Content>
        <Accordian.Content>Content-2</Accordian.Content>
      </Accordian>
    )
}
export default App;
          `}
      />
    </>
  );
};

export const Default = Template.bind({});
