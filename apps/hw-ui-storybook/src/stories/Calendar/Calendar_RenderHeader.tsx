import { Calendar, DateSelectType } from "ui";
import { StoryFn } from "@storybook/react";
import { Dispatch, SetStateAction } from "react";
import SandpackEditor from "../common/SandpackEditor";

const CalendarRenderHeader: StoryFn<typeof Calendar> = (args) => {
  const handleRenderHeader = (
    setSelectType?: Dispatch<SetStateAction<DateSelectType>>,
    selectedDate?: Date,
    prevNav?: () => void,
    nextNav?: () => void
  ) => {
    return (
      <div>
        <button onClick={() => setSelectType?.("months")}>Month</button>
        <button onClick={() => setSelectType?.("days")}>Day</button>
        <br />
        <br />
        <button onClick={prevNav}>prev</button>
        {selectedDate?.toISOString()}
        <button onClick={nextNav}>next</button>
        <br />
        <br />
      </div>
    );
  };
  return (
    <>
      <Calendar {...args} renderHeader={handleRenderHeader} />
      <br />
      <br />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  const handleRenderHeader = (
        setSelectType?: Dispatch<SetStateAction<DateSelectType>>,
        selectedDate?: Date,
        prevNav?: () => void,
        nextNav?: () => void
      ) => { 
      return (
        <div>
          <button onClick={() => setSelectType?.("months")}>Month</button>
          <button onClick={() => setSelectType?.("days")}>Day</button>
          <br />
          <br />
          <button onClick={prevNav}>prev</button>
          {selectedDate?.toISOString()}
          <button onClick={nextNav}>next</button>
          <br />
          <br />
        </div>
      );
  }
  return (
    <Calendar renderHeader={handleRenderHeader} />
  );
};
export default Example;
`}
      />
    </>
  );
};
export default CalendarRenderHeader;
