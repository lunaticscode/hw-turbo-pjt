import { Calendar } from "ui";
import { StoryFn } from "@storybook/react";
import SandpackEditor from "../common/SandpackEditor";

const CalendarLimitDate: StoryFn<typeof Calendar> = (args) => {
  return (
    <>
      <Calendar {...args} minDate={new Date()} />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  const [date, setDate] = useState<DateType>(new Date());
  const handleChangeDate = (value: DateType) => {
    setDate(value);
  };
  return (
      <Calendar value={date} minDate={new Date()} />
  );
};
export default Example;
`}
      />
      <Calendar {...args} maxDate={new Date()} />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
        import 'hw-rui/index.css';    
        import { FC, useState } from "react";
        const Example = () => {
          const [date, setDate] = useState<DateType>(new Date());
          const handleChangeDate = (value: DateType) => {
            setDate(value);
          };
          return (    
            <Calendar value={date} minDate={new Date()} />
          );
        };
        export default Example;`}
      />
      <Calendar
        {...args}
        disabledDate={(date: Date) =>
          date.getTime() < new Date("2023-06-01").getTime()
        }
      />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  const [date, setDate] = useState<DateType>(new Date());
  const handleChangeDate = (value: DateType) => {
    setDate(value);
  };
  return (
    <Calendar value={date} maxDate={(date: Date) => date.getTime() < new Date("2023-06-01").getTime()} />
  );
};
export default Example;
`}
      />
    </>
  );
};
export default CalendarLimitDate;
