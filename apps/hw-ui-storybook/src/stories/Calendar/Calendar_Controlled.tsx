import { Calendar, DateType } from "ui";
import { StoryFn } from "@storybook/react";
import { useState } from "react";
import SandpackEditor from "../common/SandpackEditor";

const CalendarControlled: StoryFn<typeof Calendar> = (args) => {
  const [date, setDate] = useState<DateType>(new Date());
  const handleChangeDate = (value: DateType) => {
    console.log({ value });
    setDate(value);
  };
  return (
    <>
      <Calendar {...args} value={date} onChange={handleChangeDate} />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
        import 'hw-rui/index.css';    
        import { FC, useState } from "react";
        const Example = () => {
          const [date, setDate] = useState<DateType>(new Date());
          const handleChangeDate = (value: DateType) => {
            setDate(value);
          };
          return (
              <Calendar value={date} onChange={handleChangeDate} />
          );
        };
        export default Example;`}
      />
    </>
  );
};
export default CalendarControlled;
