import { Calendar, DateType } from "ui";
import { StoryFn } from "@storybook/react";
import SandpackEditor from "../common/SandpackEditor";

const CalendarOnChange: StoryFn<typeof Calendar> = (args) => {
  const handleChangeDate = (value: DateType) => {
    console.log(value);
  };
  return (
    <>
      <Calendar
        {...args}
        defaultValue={new Date()}
        onChange={handleChangeDate}
      />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  return (
      <Calendar defaultValue={new Date()} />
  );
};
export default Example;
`}
      />
    </>
  );
};
export default CalendarOnChange;
