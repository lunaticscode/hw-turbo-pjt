import { Calendar } from "ui";
import { StoryFn } from "@storybook/react";
import SandpackEditor from "../common/SandpackEditor";

const CalendarRenderDateCell: StoryFn<typeof Calendar> = (args) => {
  const handleRenderDate = (date: Date) => {
    return <div>{date.toISOString().split("T")[0]}</div>;
  };
  return (
    <>
      <Calendar {...args} renderDateCell={handleRenderDate} />
      <SandpackEditor
        code={`import { Calendar, DateType } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  const handleRenderDate = (date: Date) => {
    return <div>{date.toISOString().split("T")[0]}</div>;
  };
  return (
    <Calendar renderDateCell={handleRenderDate} />
  );
};
export default Example;
`}
      />
    </>
  );
};
export default CalendarRenderDateCell;
