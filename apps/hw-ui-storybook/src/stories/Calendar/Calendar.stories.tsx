import React from "react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Calendar } from "ui";
import CalendarUncontrolled from "./Calendar_Uncontrolled";
import CalendarControlled from "./Calendar_Controlled";
import CalendarLimitDate from "./Calendar_LimitDate";
import CalendarRenderDateCell from "./Calendar_RenderDate";
import CalendarRenderHeader from "./Calendar_RenderHeader";
import SandpackEditor from "../common/SandpackEditor";

export default {
  title: "Example/Calendar",
  component: Calendar,
} as StoryObj<typeof Calendar>;

const Template: StoryFn<typeof Calendar> = (args) => {
  return (
    <>
      <Calendar {...args} />
      <SandpackEditor
        code={`import { Calendar } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  return (
      <Calendar />
  );
};
export default Example;
`}
      />
    </>
  );
};

export const Default = Template.bind({});
export const Uncontrolled = CalendarUncontrolled.bind({});
export const Controlled = CalendarControlled.bind({});
export const LimitDate = CalendarLimitDate.bind({});
export const RenderDateCell = CalendarRenderDateCell.bind({});
export const RenderHeader = CalendarRenderHeader.bind({});
