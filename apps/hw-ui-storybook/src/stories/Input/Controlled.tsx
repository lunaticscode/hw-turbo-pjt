import { Input, InputValue } from "ui";
import { StoryFn } from "@storybook/react";
import { ChangeEvent, useState } from "react";
import SandpackEditor from "../common/SandpackEditor";

const Controlled: StoryFn<typeof Input> = (args) => {
  const [value, setValue] = useState<InputValue>("");
  const handleChangeInput = (
    e: ChangeEvent<HTMLInputElement>,
    inputValue: InputValue
  ) => {
    setValue(inputValue);
  };
  return (
    <>
      <Input {...args} value={value} onChange={handleChangeInput} />
      <br />
      <br />
      <SandpackEditor
        code={`import { Input } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  const [value, setValue] = useState<string>("");  
  const handleChangeInput = (e: ChangeEvent<HTMLInputElement>, inputValue: string) => {
    setValue(inputValue);
  }
  return (
      <Input value={value} onChange={handleChangeInput} />
  );
};
export default Example;`}
      />
    </>
  );
};
export default Controlled;
