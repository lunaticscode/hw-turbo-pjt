import React from "react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Input, InputSearch } from "ui";
import InputUncontrolled from "./Uncontrolled";
import InputControlled from "./Controlled";
import SandpackEditor from "../common/SandpackEditor";

export default {
  title: "Example/Input",
  component: Input,
} as StoryObj<typeof Input & typeof InputSearch>;

const Template: StoryFn<typeof Input> = (args) => {
  return (
    <>
      <Input {...args} />
      <br />
      <Input {...args} variation="danger" />
      <br />
      <Input {...args} variation="primary" />
      <SandpackEditor
        code={`import { Input } from "hw-rui";
        import 'hw-rui/index.css';
        import { FC, useState } from "react";
        const Example = () => {
          return (
            <div>
              <Input />
              <br />
              <Input variation="danger" />
              <br />
              <Input variation="primary" />
            </div>
          );
        };
        export default Example;`}
      />
    </>
  );
};

export const Default = Template.bind({});
export const Uncontrolled = InputUncontrolled.bind({});
export const Controlled = InputControlled.bind({});
