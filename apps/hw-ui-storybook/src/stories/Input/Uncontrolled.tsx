import { Input } from "ui";
import { StoryFn } from "@storybook/react";
import SandpackEditor from "../common/SandpackEditor";
const Uncontrolled: StoryFn<typeof Input> = (args) => {
  return (
    <>
      <Input {...args} defaultValue={"default-input-value"} />
      <SandpackEditor
        code={`import { Input } from "hw-rui";
import 'hw-rui/index.css';    
import { FC, useState } from "react";
const Example = () => {
  return (
    <Input defaultValue={"default-input-value"} />
  );
};
export default Example;`}
      />
    </>
  );
};
export default Uncontrolled;
