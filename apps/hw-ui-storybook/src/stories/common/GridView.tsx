import { CSSProperties, FC, ReactNode } from "react";

const GridView: FC<{
  children?: ReactNode;
  rule?: CSSProperties["gridTemplateRows"];
}> = ({ children, rule = "1fr 1fr" }) => {
  return (
    <div
      style={{
        display: "grid",
        gridTemplateColumns: rule,
        gridColumnGap: "20px",
      }}
    >
      {children}
    </div>
  );
};

export default GridView;
