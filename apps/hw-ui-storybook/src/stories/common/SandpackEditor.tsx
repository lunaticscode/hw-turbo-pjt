import { FC } from "react";

import {
  SandpackLayout,
  SandpackPreview,
  SandpackProvider,
  SandpackCodeEditor,
} from "@codesandbox/sandpack-react";
interface CodeEditorProps {
  code?: string;
}
const SandpackEditor: FC<CodeEditorProps> = ({ code }) => {
  return (
    <div style={{ margin: "10px 0px" }}>
      <SandpackProvider
        template="react-ts"
        customSetup={{
          dependencies: { "hw-rui": "0.1.1" },
        }}
        files={{ "App.tsx": code || "" }}
      >
        <SandpackLayout style={{ marginBottom: "5px" }}>
          <SandpackPreview />
        </SandpackLayout>
        <SandpackLayout>
          <SandpackCodeEditor />
        </SandpackLayout>
      </SandpackProvider>
    </div>
  );
};

export default SandpackEditor;
