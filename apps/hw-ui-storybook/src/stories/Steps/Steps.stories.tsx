import { StoryFn, StoryObj } from "@storybook/react";
import { Source } from "@storybook/blocks";
import { Steps } from "ui";
import GridView from "../common/GridView";
import StepsUncontrolled from "./Uncontrolled";
import StepsControlled from "./Controlled";

export default {
  title: "Example/Steps",
  component: Steps,
} as StoryObj<typeof Steps>;
const stepItems = [{ label: "step1" }, { label: "step2" }, { label: "step3" }];
const Template: StoryFn<typeof Steps> = (args) => {
  return (
    <GridView>
      <div>
        <Steps {...args} items={stepItems} />
      </div>
      <div>
        <Source
          code={`
import { Steps } from 'hw-react-ui';
const StepsExample = () => {
  return (
    <Steps/>
  )
}
`}
        />
      </div>
    </GridView>
  );
};

export const Default = Template.bind({});
export const Uncontrolled = StepsUncontrolled;
export const Controlled = StepsControlled;
