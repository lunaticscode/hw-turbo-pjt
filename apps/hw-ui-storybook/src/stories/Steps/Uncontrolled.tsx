import { Steps } from "ui";
import { StoryFn } from "@storybook/react";
import { Source } from "@storybook/blocks";
import GridView from "../common/GridView";

const stepItems = [{ label: "step1" }, { label: "step2" }, { label: "step3" }];
const Controlled: StoryFn<typeof Steps> = (args) => {
  return (
    <GridView>
      <div>
        <Steps {...args} items={stepItems} defaultStepIndex={1} />
      </div>
      <div>
        <Source
          code={`
import { Steps, StepsProps } from "hw-ui-react/Input";
import { FC, useState } from "react";
        
interface StepsUncontrolledProps extends StepsProps {}

const stepItems = [{ label: "step1" }, { label: "step2" }, { label: "step3" }];        
const StepsUncontrolled: FC<StepsUncontrolledProps> = () => {
  
  return (
      <Steps items={stepItems} defaultStepIndex={1} />
  );
};
        
export default StepsUncontrolled;      
      `}
        />
      </div>
    </GridView>
  );
};
export default Controlled;
