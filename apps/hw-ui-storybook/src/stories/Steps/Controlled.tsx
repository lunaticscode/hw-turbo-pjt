import { Steps } from "ui";
import { StoryFn } from "@storybook/react";
import { Source } from "@storybook/blocks";
import GridView from "../common/GridView";
import { useState } from "react";

const stepItems = [{ label: "step1" }, { label: "step2" }, { label: "step3" }];
const Controlled: StoryFn<typeof Steps> = (args) => {
  const [stepIndex, setStepIndex] = useState<number>(0);

  const handleClickStep = (stepIndex: number) => {
    setStepIndex(stepIndex);
  };
  return (
    <GridView>
      <div>
        <Steps
          {...args}
          items={stepItems}
          stepIndex={stepIndex}
          onChange={handleClickStep}
        />
      </div>
      <div>
        <Source
          code={`
import { Steps, StepsProps } from "hw-ui-react/Input";
import { FC, useState } from "react";
        
interface StepsControlledProps extends StepsProps {}
        
const stepItems = [{ label: "step1" }, { label: "step2" }, { label: "step3" }];
const StepsControlled: FC<StepsControlledProps> = () => {
  const [stepIndex, setStepIndex] = useState<number>(0);  
  const handleClickStep = (stepIndex: number) => {
    setStepIndex(stepIndex);
  };
  return (
      <Steps items={stepItems} stepIndex={stepIndex} onClickStep={handleClickStep} />
  );
};
        
export default StepsControlled;      
      `}
        />
      </div>
    </GridView>
  );
};
export default Controlled;
