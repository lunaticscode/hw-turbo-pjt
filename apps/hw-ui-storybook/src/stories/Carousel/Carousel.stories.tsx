import React, { useRef, useState, CSSProperties } from "react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Sandpack } from "@codesandbox/sandpack-react";
import { Carousel, CarouselRefProps, Button } from "ui";
import SandpackEditor from "../common/SandpackEditor";

const getTestStyle = (axis: number): CSSProperties => {
  return {
    display: "flex",
    textAlign: "center",
    backgroundColor: `rgb(${150 + 10 * axis}, ${30 + 15 * axis}, ${
      255 - 15 * axis
    })`,
    color: "white",
    height: "100%",
    width: "100%",
    fontSize: "30px",
    fontWeight: "bold",
    justifyContent: "center",
    alignItems: "center",
  };
};

export default {
  title: "Example/Carousel",
  component: Carousel,
} as StoryObj<typeof Carousel>;

const Template: StoryFn<typeof Carousel> = (args) => {
  const carouselRef = useRef<CarouselRefProps>(null);
  return (
    <>
      <Carousel ref={carouselRef} {...args} bordered>
        {Array.from({ length: 10 }, (_, index) => (
          <Carousel.Item key={`carousel-item-${index}`}>
            <div style={getTestStyle(index + 1)}>{index}</div>x
          </Carousel.Item>
        ))}
      </Carousel>
      <Button onClick={() => carouselRef?.current?.prev()}>trigger-prev</Button>
      <Button onClick={() => carouselRef?.current?.next()}>trigger-next</Button>
      <SandpackEditor
        code={`import { useRef, CSSProperties } from 'react';         
        import 'hw-rui/index.css';    
        import { Carousel, CarouselRefProps, Button } from 'hw-rui';
        
        const getItemStyle = (index: number): CSSProperties => {
            return {
              display: "flex",
              color: "white",
              width: "100%",
              textAlign: "center",
              height: "100%",
              fontSize: '30px',
              fontWeight: 'bold',
              backgroundColor: "rgb(" + (150 + 10 * index) + ", " + (30 + 15 * index) + ", " + (255 - 15 * index) + ")",
              lineHeight: "100%",
              alignItems: "center",
              justifyContent: "center",
            };
        }
        
        const Example = () => {
          const carouselRef = useRef<CarouselRefProps>(null);
          return (
            <div>
              <Carousel ref={carouselRef}>
                <Carousel.Item>
                  <div style={getItemStyle(1)}>1</div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={getItemStyle(2)}>2</div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={getItemStyle(3)}>3</div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={getItemStyle(4)}>4</div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={getItemStyle(5)}>5</div>
                </Carousel.Item>
                <Carousel.Item>
                  <div style={getItemStyle(6)}>6</div>
                </Carousel.Item>
              </Carousel>
              <Button onClick={() => carouselRef?.current?.prev()}>trigger-prev</Button>
              <Button onClick={() => carouselRef?.current?.next()}>trigger-next</Button>
            </div> 
          )
        }
        export default Example;`}
      />
    </>
  );
};

export const Default = Template.bind({});
