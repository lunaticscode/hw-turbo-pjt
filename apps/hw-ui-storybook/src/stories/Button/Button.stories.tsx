import { StoryFn, StoryObj } from "@storybook/react";
import { Button } from "ui";
import SandpackEditor from "../common/SandpackEditor";
export default {
  title: "Example/Button",
  component: Button,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as StoryObj<typeof Button>;

const Template: StoryFn<typeof Button> = (args) => (
  <>
    <SandpackEditor
      code={`import { Button } from 'hw-rui';
import 'hw-rui/index.css';
const Example = () => {
    return (
        <div style={{ display: "flex", justifyContent: "space-around" }}>
          <Button>button</Button>
          <Button variation="primary">button</Button>
          <Button variation="danger">button</Button>
          <Button variation="ghost">button</Button>
        </div>
    )
}
export default Example;
          `}
    />
  </>
);
export const Default = Template.bind({});

// export const Primary = Template.bind({});
// // More on args: https://storybook.js.org/docs/react/writing-stories/args
// Primary.args = {
//   // primary: true,
//   // label: "Button",
// };

// export const Secondary = Template.bind({});
// Secondary.args = {
//   // label: "Button",
// };

// export const Large = Template.bind({});
// Large.args = {
//   // size: 'large',
//   // label: "Button",
// };

// export const Small = Template.bind({});
// Small.args = {
//   // size: 'small',
//   // label: "Button",
// };
