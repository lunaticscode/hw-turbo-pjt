import { StoryFn, StoryObj } from "@storybook/react";
import { Source } from "@storybook/blocks";
import { Tabs } from "ui";

import GridView from "../common/GridView";

export default {
  title: "Example/Tabs",
  component: Tabs,
} as StoryObj<typeof Tabs>;

const TabsDefaultValue = "b";
const Template: StoryFn<typeof Tabs> = (args) => {
  return (
    <GridView>
      <div>
        <Tabs defaultValue={TabsDefaultValue}>
          <Tabs.Tab value={"a"}>Tab1asdasdasdasdasd</Tabs.Tab>
          <Tabs.Tab value={"b"}>Tab2</Tabs.Tab>
          <Tabs.Tab value={"c"}>Tab3</Tabs.Tab>
          <Tabs.TabPannel>1</Tabs.TabPannel>
          <Tabs.TabPannel>2</Tabs.TabPannel>
          <Tabs.TabPannel>3</Tabs.TabPannel>
        </Tabs>
      </div>
      <div>
        <Source
          code={`
import { Tabs, Tab, TabPannel } from 'hw-react-ui';
const TabsExample = () => {
  return (
    <Tabs>
        <Tab></Tab>
        <Tab></Tab>
        <Tab></Tab>
    </Tabs>
  )
}
export default TabsExample;
`}
        />
      </div>
    </GridView>
  );
};

export const Default = Template.bind({});
