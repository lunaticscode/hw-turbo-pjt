import { Popover, PopoverPositions } from "ui";
import { StoryFn } from "@storybook/react";
import { Source } from "@storybook/blocks";
import GridView from "../common/GridView";
import { useRef, useState } from "react";

const Position: StoryFn<typeof Popover> = (args) => {
  const anchorRef = useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [position, setPosition] = useState<PopoverPositions>("bottom-left");

  return (
    <GridView>
      <div>
        <button onClick={() => setIsOpen(!isOpen)}>Popover Toggle</button>
        <br />
        <button onClick={() => setPosition("bottom-left")}>bottom-left</button>
        <button onClick={() => setPosition("bottom-right")}>
          bottom-right
        </button>
        <button onClick={() => setPosition("top-left")}>top-left</button>
        <button onClick={() => setPosition("top-right")}>top-right</button>
        <br />
        <br />
        <div ref={anchorRef} id="test-anchor">
          PopoverContainer
        </div>

        <Popover
          {...args}
          open={isOpen}
          anchorRef={anchorRef}
          anchorPosition={position}
        >
          <div>Popover Content</div>
        </Popover>
      </div>
      <div>
        <Source
          code={`
import Popover, { PopoverProps, PopoverPositions } from "hw-ui-react/Popover";
import { FC, useRef, useState } from "react";
        
interface PopoverExampleProps extends PopoverProps {}
        
const PopoverExample: FC<PopoverExampleProps> = () => {
  const anchorRef = useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [position, setPosition] = useState<PopoverPositions>("bottom-left");
  
  return (
        <div>
            <button onClick={() => setIsOpen(!isOpen)}>Popover Toggle</button>
            <br />
            <button onClick={() => setPosition("bottom-left")}>bottom-left</button>
            <button onClick={() => setPosition("bottom-right")}>
                bottom-right
            </button>
            <button onClick={() => setPosition("top-left")}>top-left</button>
            <button onClick={() => setPosition("top-right")}>top-right</button>
            <br />
            <br />
            <div ref={anchorRef} id="test-anchor">
                PopoverContainer
            </div>

            <Popover
                {...args}
                open={isOpen}
                anchorRef={anchorRef}
                anchorPosition={position}
            />
        </div>
  );
};
        
export default InputUncontrolled;      
      `}
        />
      </div>
    </GridView>
  );
};
export default Position;
