import React, { useCallback, useRef, useState } from "react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Source } from "@storybook/blocks";
import { Popover, PopoverPositions } from "ui";
import PopoverPosition from "./Position";

import GridView from "../common/GridView";

export default {
  title: "Example/Popover",
  component: Popover,
} as StoryObj<typeof Popover>;

const Template: StoryFn<typeof Popover> = (args) => {
  const anchorRef = useRef<HTMLDivElement>(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [position, setPosition] = useState<PopoverPositions>("bottom-left");
  const handleClickOutSide = useCallback(() => {
    setIsOpen(false);
  }, []);
  return (
    <GridView>
      <div>
        <button onClick={() => setIsOpen(!isOpen)}>Popover Toggle</button>
        <br />
        <button onClick={() => setPosition("bottom-left")}>bottom-left</button>
        <button onClick={() => setPosition("bottom-right")}>
          bottom-right
        </button>
        <button onClick={() => setPosition("top-left")}>top-left</button>
        <button onClick={() => setPosition("top-right")}>top-right</button>
        <br />
        <br />
        <div ref={anchorRef} id="test-anchor">
          PopoverContainer
        </div>

        <Popover
          {...args}
          open={isOpen}
          anchorRef={anchorRef}
          anchorPosition={position}
          onClickOutside={handleClickOutSide}
        >
          <div>Popover Content</div>
        </Popover>
      </div>
      <div>
        <Source
          code={`
import { Popover } from 'hw-react-ui';
const PopoverExample = () => {
  return (
    <Popover/>
  )
}
`}
        />
      </div>
    </GridView>
  );
};

export const Default = Template.bind({});
export const Position = PopoverPosition.bind({});
