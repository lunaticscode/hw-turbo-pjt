import React from "react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Breadcrumb, BreadcrumbItemProps } from "ui";
import SandpackEditor from "../common/SandpackEditor";

export default {
  title: "Example/Breadcrumb",
  component: Breadcrumb,
} as StoryObj<typeof Breadcrumb>;

const BreadcrumbItems: Array<BreadcrumbItemProps> = [
  { label: "A", href: "#A" },
  { label: "B", href: "#B" },
  { label: "C", href: "#C" },
];

const Template: StoryFn<typeof Breadcrumb> = (args) => {
  const handleClickCrumb = (href: string | undefined) => {
    console.log(href);
  };
  return (
    <>
      <Breadcrumb
        {...args}
        onClickCrumb={handleClickCrumb}
        items={BreadcrumbItems}
      />

      <SandpackEditor
        code={`import { Breadcrumb, BreadcrumbItemProps } from 'hw-rui';
import "hw-rui/index.css";

const BreadcrumbItems: Array<BreadcrumbItemProps> = [
  { label: "A", href: "#A" },
  { label: "B", href: "#B" },
  { label: "C", href: "#C" },
];

const BreadcrumbExample = () => {
    return (
      <Breadcrumb items={BreadcrumbItems} /> 
    )
}
export default BreadcrumbExample;
`}
      />
    </>
  );
};

export const Default = Template.bind({});
