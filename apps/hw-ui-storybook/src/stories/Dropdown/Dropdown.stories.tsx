import { Sandpack } from "@codesandbox/sandpack-react";
import { StoryFn, StoryObj } from "@storybook/react";
import { Dropdown } from "ui";
import SandpackEditor from "../common/SandpackEditor";

export default {
  title: "Example/Dropdown",
  component: Dropdown,
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as StoryObj<typeof Dropdown>;

const Template: StoryFn<typeof Dropdown> = (args) => (
  <>
    <SandpackEditor
      code={`import { Dropdown } from "hw-rui";
      import 'hw-rui/index.css';
      const Example = () => {
        return (
          <Dropdown>
            <Dropdown.Button>D-Button</Dropdown.Button>
            <Dropdown.Item>D-Item</Dropdown.Item>
            <Dropdown.Item>D-Item</Dropdown.Item>
            <Dropdown.Item>D-Item</Dropdown.Item>
          </Dropdown>
        );
      };
      export default Example;`}
    />
  </>
);
export const Default = Template.bind({});

// export const Primary = Template.bind({});
// // More on args: https://storybook.js.org/docs/react/writing-stories/args
// Primary.args = {
//   // primary: true,
//   // label: "Button",
// };

// export const Secondary = Template.bind({});
// Secondary.args = {
//   // label: "Button",
// };

// export const Large = Template.bind({});
// Large.args = {
//   // size: 'large',
//   // label: "Button",
// };

// export const Small = Template.bind({});
// Small.args = {
//   // size: 'small',
//   // label: "Button",
// };
