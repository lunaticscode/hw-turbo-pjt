"use client";

import { useEffect } from "react";

const AppLoading = () => {
  // const setLayoutStatus = loadingStore(store => store.setLayoutStore);
  useEffect(() => {
    // setLayoutStatus({loading: true})
    console.log("AppLoading mounted");
    return () => {
      // setLayoutStatus({loading: false})
      console.log("unmount");
    };
  }, []);
  return <div className={"app-loading"} />;
};
export default AppLoading;
