import { NextPage } from "next";
import { notFound } from "next/navigation";
import { useMemo } from "react";

const validComponents = ["button"];

type ComponentSlug = { component: string[] };
const ComponentSlugPage: NextPage<{ params: ComponentSlug }> = ({
  params: { component },
}) => {
  if (!validComponents.includes(component[0])) {
    notFound();
  }
  const pageTitle = useMemo(
    () => component?.[0][0].toUpperCase() + component[0].slice(1),
    [component]
  );
  return <h2>{pageTitle}</h2>;
};
export default ComponentSlugPage;
