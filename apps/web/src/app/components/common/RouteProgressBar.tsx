import { FC } from "react";

interface RoutProgressBarProps {
  isLoading?: boolean;
}
const RoutProgressBar: FC<RoutProgressBarProps> = ({ isLoading }) => {
  return isLoading && <div>loading...</div>;
};
export default RoutProgressBar;
