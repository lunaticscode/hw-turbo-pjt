"use client";

import { FC, PropsWithChildren, Suspense, useMemo } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Sidebar from "./Sidebar";
import { usePathname } from "next/navigation";
import Main from "./Main";

const sidebarUnvisiblePaths = ["/"];

interface LayoutProps extends PropsWithChildren {}
const Layout: FC<LayoutProps> = ({ children }) => {
  const pathname = usePathname();
  const withSidebar = useMemo(
    () => !sidebarUnvisiblePaths.includes(pathname),
    [pathname]
  );
  return (
    <>
      <Header />
      {withSidebar && <Sidebar />}
      <Main withSidebar={withSidebar}>{children}</Main>
      <Footer />
    </>
  );
};
export default Layout;
