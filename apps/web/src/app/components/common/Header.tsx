import Link from "next/link";
import { FC, useMemo } from "react";
import cls from "classnames";
import { usePathname } from "next/navigation";

type NavMenu = { href: string; label: string };

export const navMenus: NavMenu[] = [
  { href: "/", label: "Concept" },
  { href: "/foundation", label: "Foundation" },
  { href: "/component", label: "Component" },
  { href: "/data", label: "Data" },
];

const navMenusWrapperCls = cls("nav-menus-wrapper");
const navMenuCls = cls("nav-menu");
const logoWrapperCls = cls("logo-wrapper");

const Header: FC = () => {
  const pathname = usePathname();
  const rootPathname = useMemo(() => "/" + pathname.split("/")[1], [pathname]);

  return (
    <header>
      <div className={logoWrapperCls}>
        <Link href="/">{`<HW-RUI/>`}</Link>
      </div>
      <ul className={navMenusWrapperCls}>
        {navMenus.map(({ href, label }, index) => (
          <li
            key={`nav-menu-${index}`}
            className={cls(navMenuCls, { active: rootPathname === href })}
          >
            <Link href={href}>{label}</Link>
          </li>
        ))}
      </ul>
    </header>
  );
};
export default Header;
