import { FC, PropsWithChildren, useMemo } from "react";
import cls from "classnames";
interface MainProps extends PropsWithChildren {
  withSidebar?: boolean;
}
const Main: FC<MainProps> = ({ children, withSidebar }) => {
  const mainCls = useMemo(
    () => cls({ "with-sidebar": withSidebar }),
    [withSidebar]
  );
  return <main className={mainCls}>{children}</main>;
};
export default Main;
