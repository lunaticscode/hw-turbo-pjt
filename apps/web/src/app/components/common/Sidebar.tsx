import { usePathname } from "next/navigation";
import { FC, useMemo } from "react";
import Link from "next/link";
import cls from "classnames";
import { navMenus } from "./Header";

type SidebarMenu = { href: string; label: string };
const foundationMenus: SidebarMenu[] = [
  {
    href: "/overview",
    label: "Overview",
  },
  { href: "/color", label: "Color" },
];

const componentMenus: SidebarMenu[] = [
  {
    href: "/button",
    label: "Button",
  },
];

const dataMenus: SidebarMenu[] = [
  {
    href: "/overview",
    label: "Overview",
  },
];

type NavMenuPaths = "/foundation" | "/component" | "/data";
const mapNavMenuToSidebarMenus: Record<NavMenuPaths, SidebarMenu[]> = {
  "/foundation": foundationMenus,
  "/component": componentMenus,
  "/data": dataMenus,
};

const sidebarMenusWrapperCls = cls("sidebar-menus-wrapper");
const sidebarMenuCls = cls("sidebar-menu");
const Sidebar: FC = () => {
  const pathname = usePathname();
  const rootPath = useMemo(() => "/" + pathname.split("/")[1], [pathname]);
  const [sidebarMenus, rootMenu, currentMenuPath] = useMemo(
    () => [
      mapNavMenuToSidebarMenus[rootPath as NavMenuPaths],
      navMenus.find(({ href }) => href === `${rootPath}`)?.label,
      pathname.split("/")[2] || "",
    ],
    [pathname, rootPath]
  );

  return (
    <aside>
      <div className={sidebarMenusWrapperCls}>
        <div className={cls(sidebarMenuCls, "root")}>
          <Link href={rootPath}>{rootMenu}</Link>
        </div>
        <ul>
          {sidebarMenus.map(({ href, label }, index) => (
            <li
              className={cls(sidebarMenuCls, {
                active: "/" + currentMenuPath === href,
              })}
              key={`sidebar-menu-${index}`}
            >
              <Link href={`${rootPath}${href}`}>{label}</Link>
            </li>
          ))}
        </ul>
      </div>
    </aside>
  );
};
export default Sidebar;
