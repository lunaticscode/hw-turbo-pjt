const sleep = async () =>
  await new Promise<void>((resolve) => setTimeout(() => resolve(), 2000));

const ConceptPage = async () => {
  // await sleep();
  return <h2>Concept</h2>;
};
export default ConceptPage;
